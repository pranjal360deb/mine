import { Component, OnInit } from '@angular/core';
import {ContentFetchService} from '../services/content-fetch.service'
import {DashService} from '../dashboard/dash.service'
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataSource } from '@angular/cdk/table';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss']
})
export class PeopleListComponent implements OnInit {

  filterForm: FormGroup;
  page=0
  historypage=0
  peopleList=[]
  count
  pagesall = 1;
  pagggee = 25
  closeResult='';
  userId
  detailsLogin=[]
  activeWeb
  activeMob
  lastLogin
  lastAlert
  pageNo = [
    25,
    50,
    75,
    100
 ];
 historypageNo = [
  25,
  50,
  75,
  100
];
 pageValue=25;
 hiscount;
 creationList

 videoTotalCount
 videoReportCount

 photoTotalCount
 photoReportCount

 storyTotalCount
 storyReportCount


 musicTotalCount
 musicReportCount

 podcastTotalCount
 podcastReportCount

 askTotalCount
 askReportCount

 articlesTotalCount
 articlesReportCount

 notesTotalCount
 notesReportCount

 thoughtTotalCount
 thoughtReportCount
 historyDetails=[]
 adConfirm
 blockId
 blockValue
 blockForm: FormGroup;
 name
 serachPeople=''
 search=[
   {id:0,name:'Mobile'},
   {id:1,name:'Email'},
   {id:2,name:'Name'},

 ]
 criteria
 searchstring
 baseURL
  constructor(private formBuilder: FormBuilder,private toastr: ToastrService,private modalService: NgbModal,private ContentFetchService: ContentFetchService, private DashService:DashService) { }

  ngOnInit(): void {
    this.baseURL=environment.baseURL
    this.getpeople();
    this.userCount();
    this.filterForm = this.formBuilder.group({
      criteria: 0,
      searchstring: '',
 
    });
  }
  get f() { return this.filterForm.controls; }
  filter() {
    this.criteria = this.f.criteria.value;
    // console.log( this.criteria)
    this.serachPeople = this.f.searchstring.value;
    this.getpeople();

  }
  onSearchChange(value){

    this.serachPeople=value;
    // console.log(value);
    this.getpeople();
  }
  FieldsChange(event,id){
    //  this.filterForm.get('ads').setValue(event);
    this.adConfirm=event.currentTarget.checked;
        // console.log(this.adConfirm)\\
        // console.log(id)
        this.blockId=id;
// console.log(event.currentTarget.checked);
this.blockUser()
  }
  blockUser(){
    this.ContentFetchService.blockUser(this.blockId,this.adConfirm).subscribe(data=>{
      // console.log(data);
      this.toastr.success('Update Successfully', 'User Status', {
        timeOut: 3000
    });
    })
  }
  loadPage(pageNumber: number) {
    // if (page !== this.previousPage) {
    //   this.previousPage = page;
    //   this.loadData();
    // this.isLoading=false;
    this.peopleList=[];
    this.page = pageNumber - 1;
    // }
    this.getpeople()
    // console.log(this.page);
  }

  loadHistory(pageNumber: number) {
    // if (page !== this.previousPage) {
    //   this.previousPage = page;
    //   this.loadData();
    // this.isLoading=false;
    this.historyDetails=[];
    this.historypage = pageNumber - 1;
    // }
    this.history()
    // console.log(this.page);
  }
  userCount(){
    this.DashService.getUser().subscribe(data=>{
      this.count=data
    })
  }
  onOptionsSelected(event){
    this.pagggee=0
    this.pagggee = event.target.value;
    this.page=0
  //   this.pages=[]
  //   this.getContentType()
  //   this.pagsize = event.target.value;
  //   this.pagenum = this.videoCount/parseInt(this.pagsize);
  //   let num = parseInt(this.pagenum)
    
  //   this.pageList();
   this.getpeople()
    // console.log(value);
 
  }

  onOptionsSelectedPage(event){
    this.pageValue=0
    this.pageValue = event.target.value;
    this.historypage=0
  //   this.pages=[]
  //   this.getContentType()
  //   this.pagsize = event.target.value;
  //   this.pagenum = this.videoCount/parseInt(this.pagsize);
  //   let num = parseInt(this.pagenum)
    
  //   this.pageList();
   this.history()
    // console.log(value);
 
  }

  getpeople(){
    this.ContentFetchService.getPeople(this.page,this.pagggee,this.serachPeople ,this.criteria).subscribe(data=>{
      this.peopleList= data;
    })
  }

  loginList(){

    this.ContentFetchService.loginDtails(this.userId).subscribe(data=>{

      // this.detailsLogin=data\
      if(data.loginDetails.isActiveOnWeb==true)
{     
  this.activeWeb= 'YES'
}     else{
  this.activeWeb= 'NO'
}
if(data.loginDetails.isActiveOnMobile==true) {
  this.activeMob='YES'

}
else{
  this.activeMob='NO'

}

     this.lastLogin=data.loginDetails.lastLogin
     this.lastAlert=data.loginDetails.lastAlert

      // console.log(data)
    })
  }

  history(){
    this.ContentFetchService.userHishtory(this.userId,this.historypage,this.pageValue).subscribe(data=>{
      this.historyDetails=data
    })
  }
  historyCount(){
    this.ContentFetchService.historyCount(this.userId).subscribe(data=>{
      this.hiscount=data
      // console.log(data)\
    })
  }

  userHistoryDetails(contenttt,id) {
    this.userId=id;
    this.historyCount();
    this.history();
    // console.log(this.userId);
    // this.deleteId=reasonID;
    // this.deleteThought(id);
    // console.log( this.reasonId);
    // const modalRef = this.modalService.open(contenttt, { windowClass : "myCustomModalClass",size: 'lg', backdrop: 'static' });

    this.modalService.open(contenttt, {windowClass : "history",ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getHistoryDismissReasonl(reason)}`;
    });
   
  }

  private getHistoryDismissReasonl(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }




  loginDetails(contenttt,id) {
    this.userId=id;
    this.loginList();
    // console.log(this.userId);
    // this.deleteId=reasonID;
    // this.deleteThought(id);
    // console.log( this.reasonId);
    // const modalRef = this.modalService.open(contenttt, { windowClass : "myCustomModalClass",size: 'lg', backdrop: 'static' });

    this.modalService.open(contenttt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonl(reason)}`;
    });
   
  }

  private getDismissReasonl(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  CreationList(){
    this.ContentFetchService.creationDtails(this.userId).subscribe(data=>{

      this.videoTotalCount=data.videos.totalContent
      this.videoReportCount=data.videos.reportedContent

      this.photoTotalCount=data.images.totalContent
      this.photoReportCount=data.images.reportedContent

      this.storyTotalCount=data.stories.totalContent
      this.storyReportCount=data.stories.reportedContent

      this.musicTotalCount=data.music.totalContent
      this.musicReportCount=data.music.reportedContent

      this.podcastTotalCount=data.podcast.totalContent
      this.podcastReportCount=data.podcast.reportedContent

      this.askTotalCount=data.asks.totalContent
      this.askReportCount=data.asks.reportedContent

      this.articlesTotalCount=data.articles.totalContent
      this.articlesReportCount=data.articles.reportedContent

      this.notesTotalCount=data.notes.totalContent
      this.notesReportCount=data.notes.reportedContent
      this.thoughtTotalCount=data.thoughts.totalContent
      this.thoughtReportCount=data.thoughts.reportedContent

      // console.log(this.videoTotalCount)
    })
  }
  delete(contenttt,id) {
    this.userId=id;
    this.CreationList();
    // console.log(this.userId);
    // this.deleteId=reasonID;
    // this.deleteThought(id);
    // console.log( this.reasonId);
    // const modalRef = this.modalService.open(contenttt, { windowClass : "myCustomModalClass",size: 'lg', backdrop: 'static' });

    this.modalService.open(contenttt, {windowClass : "myCustomModalClass",ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDeleteDismissReasonl(reason)}`;
    });
   
  }

  private getDeleteDismissReasonl(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
