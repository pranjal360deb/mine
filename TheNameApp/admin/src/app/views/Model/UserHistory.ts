export interface UserHistory {
    historyId: any,
    corelationId: any
    categoryId: any,
    content: any,
    category: any,
    contentType: any,
    contentId: any,
    datetime: any
}