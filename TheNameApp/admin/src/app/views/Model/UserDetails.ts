export interface UserDetails {
    about: string,
        appsiteId: number,
        appsitePicture: {
            appsiteId: any
            appsitePictureId: any
            blobId: any
            contentType: any
            datetime: any
            extension: any
        },
        corelationId: number,
        firstName: string,
        lastName: string,
        name: string,
        tagLine: string,
        networkDetails: {appsiteId: any, believed: any, believeing: any}
        birthDate: string,
        gender: number,
        country: number,
        city: number,
        createdOn: string,
        createdBy: any,
        updatedOn: any,
        updatedBy: null,
        creations: any,
        user: {
            userId: any,
            mobileNo: any,
            emailId: any,
            isVerified: any,
            isAcceptedPrivacyPolicy: any,
            createdOn: any,
            updatedOn: any,
            updatedby: any
        },
        userId:any,
}