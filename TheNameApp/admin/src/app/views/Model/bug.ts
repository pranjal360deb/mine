export interface Bug{
    bugId: number,
    corelationId: number,
    issue: any,
    datetime: any,
    bugStatus: {
        bugStatusId: any,
        status: any,
        description: any
      }
    user: {
        corelationId: any,
        fullName: any,
        atName: any
    },
    medias: [
        {
            blobId: any,
            contentType: any
        },
     ]
}