export enum ContentTypeDetails{
    Image,
    Video,
    Podcast,
    Music,
    Story,
    Article,
    Ask,
    Poll,
    Note,
    Thought
}

export enum ReportedTypeDetails{
    All,
    IsReported,
    IsDeleted,
    IsVarified,
}