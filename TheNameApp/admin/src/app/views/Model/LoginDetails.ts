export interface LoginDetails {
    deviceDetails: [],
    loginDetails: {
        loginId: any,
        appsiteId: any,
        lastLogin: any,
        isActiveOnWeb: any,
        isActiveOnMobile: any,
        lastAlert: any
    }
}