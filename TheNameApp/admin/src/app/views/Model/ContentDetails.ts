export interface ContentDetails{
    contentId: number,
    corelationId: number,
    contentType: number,
    dateTime: any,
    reported: any,
    title: any,
    mediaIds: any,
    medias: [
        {
            blobId: any,
            contentType: any
        }
    ],
    reason: any,
reasonId: any,
    textField: string,
    thumbnail: {
        blobId: any,
        contentType: any
    },
    user: {
        corelationId: number,
        fullName: string,
        atName: string
    }
    isVerified: any,
    isDeleted: any
}