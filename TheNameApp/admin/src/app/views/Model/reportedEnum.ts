export enum ReportedTypeDetails{
    All,
    IsReported,
    IsDeleted,
    IsVarified,
}