export interface CreationDetails {
    videos: {
        totalContent: any,
        reportedContent: any
    },
    music: {
        totalContent: any,
        reportedContent: any
    },
    podcast: {
        totalContent: any,
        reportedContent: any
    },
    images: {
        totalContent: any,
        reportedContent: any
    },
    stories: {
        totalContent: any,
        reportedContent: any
    },
    thoughts: {
        totalContent: any,
        reportedContent: any
    },
    articles: {
        totalContent: any,
        reportedContent: any
    },
    notes: {
        totalContent: any,
        reportedContent: any
    },
    asks: {
        totalContent: any,
        reportedContent: any
    }
}