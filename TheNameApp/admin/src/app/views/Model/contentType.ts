
export interface ContentDetails{
    totalCount: number,
    reportedCount: number,
    contentType: number
}