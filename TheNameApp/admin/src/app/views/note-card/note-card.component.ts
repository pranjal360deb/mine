import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ContentTypeDetails } from '../Model/contentEnum';
import { DatePipe } from '@angular/common';
import {ContentFetchService} from '../services/content-fetch.service'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import{ManageDeleteService} from '../manage-delete-table/service/manage-delete.service'
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-note-card',
  templateUrl: './note-card.component.html',
  styleUrls: ['./note-card.component.scss']
})
export class NoteCardComponent implements OnInit {

  @Input() notes;
  note:any;
  imageString:any ;
  videoString:any;
  images:any
blobIDs:any;
hide: boolean = false;
open: boolean = false;
image: boolean = false;
closeResult = '';
deleteId:number;
openModal:boolean = false;
type:any;
deleting;
reasonForDelete;
allreasons=[]
deleteForm: FormGroup;
img =[];
nameValue=''
contentId:any;
  reportListUser=[];
  isdisable:boolean = false;
  constructor(private toastr: ToastrService,private ManageDeleteService: ManageDeleteService,private formBuilder: FormBuilder,private modalService: NgbModal, private ContentFetchService:ContentFetchService) { }

  ngOnInit(): void {
    if(this.notes.reasonId==null) {
      this.isdisable= true
    }
    else{
      this.isdisable= false
    }
    this.note=this.notes;
    this.blob();
    this.deleteForm = this.formBuilder.group({
      // FromDate: '',
      // ToDate: '',
      // reported: [],
      // name:'',
      // corelationId:'',
      reasonId:0,
      // delete:false
    });
  }
  reason(){
    this.ManageDeleteService.getDeleteReason().subscribe(data=>{
      this.allreasons=data
    })
  }
  openthumb(){
    if(this.hide==false){
      this.hide=true;
    }
    else{
      this.hide=false;
    }
  }

  filter(){
    // this.deleting=this.f.delete.value;
    
    this.reasonForDelete=this.f.reasonId.value;
    this.delete();
  }
  get f() { return this.deleteForm.controls; }
  blob(){
    
    this.notes.medias.forEach(element => {
      this.blobIDs= element;
      this.type=this.blobIDs.contentType;
      if(this.type=="image/png" || this.type=="image/jpeg"){
        this.imageString= environment.baseURL+"/BlobFile/"+this.blobIDs.blobId+"?ContentType="+this.blobIDs.contentType;
        this.img.push(this.imageString);
        this.open=true;
      }
      else if(this.type=="video/mp4"){
        this.images=environment.baseURL+"/BlobFile/"+this.notes.thumbnail.blobId+"?ContentType="+this.notes.thumbnail.contentType;
        this.videoString=environment.baseURL+ "/BlobFile/"+this.blobIDs.blobId+"?ContentType="+this.blobIDs.contentType;
        this.image=true;
      }
    });


    // console.log(this.notes.thumbnail.blobId)

  }

  opening(content,id,boolValue,name,value?:any) {
    this.reason()

    this.deleteId=id;
    this.deleting = boolValue;
    this.nameValue=name
    this.deleteForm.get('reasonId').setValue(value);
    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  verify(contentt,id) {
    this.deleteId=id;
    // this.deleteThought(id);

    this.modalService.open(contentt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonal(reason)}`;
    });
   
  }

  delete(){
    // this.ContentFetchService.deleteContent(ContentTypeDetails.Note,this.deleteId).subscribe(data=>{
    //   // console.log('susscefully deleted'+data);
    //   // this.openModal=true;
    //   this.modalService.dismissAll();

    // })

    this.ContentFetchService.deletingContent(ContentTypeDetails.Note, this.deleteId,this.deleting,this.reasonForDelete).subscribe(data=>{
      // console.log(data);
      this.modalService.dismissAll()
      this.toastr.success(this.nameValue+' Successfully', 'Your Content', {
        timeOut: 3000
    });
    })
    // console.log(this.deleteId);
    // this.openModal=true;
  }

  verifying(){
    this.ContentFetchService.UpdateContent(ContentTypeDetails.Note,this.deleteId).subscribe(data=>{
      // console.log('susscefully deleted'+data);
      // this.openModal=true;
      this.modalService.dismissAll();
      this.toastr.success('Verify Successfully', 'Your Content', {
        timeOut: 3000
    });
    })

  }
  private getDismissReasonal(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  reportContent(){
    this.ContentFetchService.report(ContentTypeDetails.Note,this.contentId).subscribe(data=>{
      // console.log(data)
      this.reportListUser=data
    });
  }
  reportUser(reportUserList,id) {
    this.contentId=id;
    this.reportContent();
    // this.deleteId=id;
    // this.reason();
    // this.deleting = boolValue;
    // this.nameValue=name
    // console.log(boolValue);
    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(reportUserList, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonReported(reason)}`;
    });
   
  }
  private getDismissReasonReported(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
