import { Component, OnInit,Input } from '@angular/core';
import { UserDetailsService } from './user-details.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
@Input() userDetails:any;

  public userDetail:any;
  constructor() { }

  ngOnInit(): void {

    this.userDetail=this.userDetails;
    // this.userDetail.getUser().subscribe(data=>{
    //   this.userDetails=data;
    // })
  }

}
