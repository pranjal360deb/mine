import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserDetails } from './user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserDetailsService {

  constructor(private http: HttpClient) { }
  public askURL: any ="/assets/data/user.json";

  getUser() :Observable<UserDetails[]>{
    return this.http.get<UserDetails[]>(this.askURL);
  }
}


// import { Injectable } from '@angular/core';import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable, of, throwError } from 'rxjs';import { catchError, tap, map } from 'rxjs/operators';
// import { UserDetails } from './user';
// @Injectable({  providedIn: 'root',})export class ProductService {  private productsUrl = 'api/products';
//   constructor(private http: HttpClient) { }
//   getProducts(): Observable<UserDetails[]> {    
//     return this.http.get<UserDetails[]>(this.productsUrl).pipe(        
//       tap(data => console.log(JSON.stringify(data))),        
//       catchError(this.handleError)      );  
  
//   }
//   createProduct(UserDetails: UserDetails)
//   {    
//     const headers = new HttpHeaders({ 'Content-Type': 'application/json' });  
//   } 
//      // Product Id must be null for the Web API to assign an Id    const newProduct = { ...product, id: null };    return this.http.post<Product>(this.productsUrl, newProduct, { headers })      .pipe(        tap(data => console.log('createProduct: ' + JSON.stringify(data))),        catchError(this.handleError)      );  }
//   deleteProduct(id: number): Observable<{}> {    

//     const headers = new HttpHeaders({ 'Content-Type': 'application/json' });    
//     const url = `${this.productsUrl}/${id}`;    return this.http.delete<UserDetails>(url, { headers })      .pipe(        tap(data => console.log('deleteProduct: ' + id)),        catchError(this.handleError)      );  }
//   updateProduct(product: UserDetails): Observable<UserDetails> {    
//     const headers = new HttpHeaders({ 'Content-Type': 'application/json' });    
//     const url = `${this.productsUrl}/${product.id}`;    
//     return this.http.put<UserDetails>(url, product, { headers }).pipe(        
//       tap(() => console.log('updateProduct: ' + product.id))       // Return the product on an update        map(() => product),        catchError(this.handleError)      );
//   }
//   }

