import { Component, OnInit } from '@angular/core';
import { ContentFetchService } from '../services/content-fetch.service';
import { DashService } from '../dashboard/dash.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-idea',
  templateUrl: './idea.component.html',
  styleUrls: ['./idea.component.scss']
})
export class IdeaComponent implements OnInit {
  filterForm: FormGroup;

  public ideas =[]
  page: number = 0;
  isLoading:boolean=false;
  isRefresh:boolean=false;
  pagesall = 1;
  collectionSize =100;
  pagggee = 25

  pageNo = [
    25,
    50,
    75,
    100
 ];
 ideaCounting:number;
 userList=[];
 coID:any
  fromdate:any
  todate:any

  constructor(private datePipe: DatePipe,private formBuilder: FormBuilder,private dashservice: DashService ,public ContentFetchService: ContentFetchService) { }

  ngOnInit(): void {
    this.getIdea()
    this.getIdeacount()
    this.filterForm = this.formBuilder.group({
      FromDate: '',
      ToDate: '',
      reported: [],
      name:'',
      corelationId:'',
    });
  }

  selectUser(id,name){

    this.filterForm.get('corelationId').setValue(id);
    this.filterForm.get('name').setValue(name);

  }
  onSearchChange(searchValue: string): void {  
    // console.log(searchValue);
    this.ContentFetchService.getUser(searchValue).subscribe(data=>{
      this.userList=data
      // console.log(data);
    })
  }
  get f() { return this.filterForm.controls; }

  filter(){
    this.isLoading=false;
    this.ideas=[];
    this.coID = this.f.corelationId.value;
    let fd = this.f.FromDate.value;
    // let date =this.datePipe.transform(fd,'yyyy-dd-MM');
    this.fromdate = this.datePipe.transform(fd,'yyyy-MM-dd');
    let td = this.f.ToDate.value;
    this.todate=this.datePipe.transform(td,'yyyy-MM-dd');;
    this.page=0;
    this.getIdea();
  }

  refresh(){
    this.isRefresh=true;
    this.ideas=[];
    this.isLoading=false;
    this.getIdea();
  }

  loadPage(pageNumber: number) {
    // if (page !== this.previousPage) {
    //   this.previousPage = page;
    //   this.loadData();
    this.isLoading=false;
    this.ideas=[];
    this.page = pageNumber - 1;
    // }
    this.getIdea()
    // console.log(this.page);
  }

  getIdea(){
    // if(this.page==0)
    // { 
      this.ContentFetchService.ideaGet(this.page,this.fromdate,this.todate,this.coID,this.pagggee).subscribe(data=>{
        this.isLoading=true;
        this.isRefresh=false;
        this.ideas=data;
      })
  //  }
  //    else{
  //     this.ContentFetchService.feedbackGet(this.page).subscribe(data=>{
  //       this.userDetails=this.userDetails.concat(data);;
  //     })
   
  //    }
  }

  getIdeacount(){
    this.dashservice.getIdeaCount().subscribe(data=>{
      this.ideaCounting=data;
    });
  }

  onOptionsSelected(event){
    this.pagggee=0
    this.pagggee = event.target.value;
    this.page=0
  //   this.pages=[]
  //   this.getContentType()
  //   this.pagsize = event.target.value;
  //   this.pagenum = this.videoCount/parseInt(this.pagsize);


  //   let num = parseInt(this.pagenum)
    
  //   this.pageList();
   this.getIdea();
    // console.log(value);
 
  }

}
