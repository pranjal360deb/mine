import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import {Role} from '../Model/Roles'
import { Observable, throwError } from 'rxjs';
import { UserList } from '../Model/UserList';
import { GetAdminDetails } from '../Model/GetAdminDetails';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersCreateService {

  URL= environment.baseURL+'/Admin'
  constructor(private http: HttpClient) { }
  getRoles() :Observable<Role[]>{
    let userURL= environment.baseURL+ "/Admin/GetRoles";
      return this.http.get<Role[]>(userURL);
    }

    getAdminDetail() :Observable<GetAdminDetails>{
      let userURL= environment.baseURL+ "/Admin/GetAdminDetail";
        return this.http.get<GetAdminDetails>(userURL);
      }

      changePassword(existingPassword,newPassword) :Observable<any>{
        let formData = new FormData
        formData.append("ExistingPassword",existingPassword)
        formData.append("NewPassword",newPassword)
        let userURL= environment.baseURL+"/Admin/UpdateExistingPassword";
          return this.http.post<any>(userURL,formData);
        }

    createUser(Username,Password,RoleId,FirstName,EmailId,active,LastName?:any,PhoneNo?:any) :Observable<any>{
      let formData = new FormData();
      formData.append("Username",Username)
      formData.append("Password",Password)
      formData.append("RoleId",RoleId)
      formData.append("FirstName",FirstName)
      formData.append("LastName",LastName)
      formData.append("PhoneNo",PhoneNo)
      formData.append("EmailId",EmailId)
      formData.append("IsActive",active)

      let userURL= environment.baseURL+ "/Admin";
        return this.http.post<any>(userURL,formData);
      }

      getUserList(pageNo,PageSize?:any) :Observable<UserList[]>{
        let userURL=  environment.baseURL+"/Admin/"+pageNo+'?PageSize='+PageSize;
        return this.http.get<UserList[]>(userURL);
      }

      getCount() :Observable<any>{
        let userURL= environment.baseURL+ "/Admin/AdminCount";
        return this.http.get<any>(userURL);
      }

      updateUser(AdminId,UserId,active, Username,RoleId,FirstName,LastName,PhoneNo,EmailId,Password?:any) :Observable<any>{
        let mainURL =this.URL+'/'+AdminId+'?UserId='+UserId+"&Username="+Username+"&Password="+Password+"&RoleId="+RoleId+"&FirstName="+FirstName+"&LastName="+LastName+"&PhoneNo="+PhoneNo+"&EmailId="+EmailId+'&IsActive='+active;
        return this.http.put<any>(mainURL,UserId);
      }
      Delete(pageNo) :Observable<any>{
        
        let userURL= environment.baseURL+'/Admin/?AdminId='+pageNo;
        return this.http.delete<any>(userURL,pageNo);
      }
}
