import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {UsersCreateService} from './services/users-create.service'
import { from } from 'rxjs';
@Component({
  selector: 'app-create-users',
  templateUrl: './create-users.component.html',
  styleUrls: ['./create-users.component.scss']
})
export class CreateUsersComponent implements OnInit {
  filterForm: FormGroup;
  myControl = new FormControl();
  Message:any
  description:any;
  closeResult = '';
  AdminId=1;
  AlertType=0
  roles=[]
  userName;
  passwd;
  roleID;
  fName;
  lName;
  phone;
  email;
  submitted = false;
active
  constructor(private UsersCreateService:UsersCreateService, private toastr: ToastrService, private modalService: NgbModal,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.filterForm = this.formBuilder.group({
      Username:['',[Validators.required]],
      Password:['',[Validators.required, Validators.minLength(6)]],
      RoleId:['',[Validators.required]],
      FirstName:['',[Validators.required]],
      LastName:['',[Validators.required]],
      PhoneNo:['',[Validators.required]],
      EmailId:['',[Validators.required]], 
      IsActive: true
    });
  }
  
  get f() { return this.filterForm.controls; }
  filter(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.filterForm.invalid) {
        return;
    }else
   { 
    this.userName = this.f.Username.value;
    this.passwd = this.f.Password.value;
    this.roleID = this.f.RoleId.value;
    this.fName = this.f.FirstName.value;
    this.lName = this.f.LastName.value;
    this.phone = this.f.PhoneNo.value;
    this.email = this.f.EmailId.value;
    this.active = this.f.IsActive.value;
    this.create();
    // console.log(this.userName,this.passwd,this.roleID,this.fName, this.lName, this.phone,this.email)
}
    // this.repor = rep;
    // this.description = this.f.Description.value; 
    // console.log("this is reason"+this.repor);
    // console.log("this is reason"+fd);
    // this.post()
  }
  create(){
    this.UsersCreateService.createUser(this.userName,this.passwd,this.roleID,this.fName,this.email,this.active,this.lName,this.phone).subscribe(data=>{
      // console.log(data);
      this.modalService.dismissAll();
      this.filterForm.get('Username').setValue('');
      this.filterForm.get('Password').setValue('');
      this.filterForm.get('RoleId').setValue('');
      this.filterForm.get('FirstName').setValue('');
      this.filterForm.get('LastName').setValue('');
      this.filterForm.get('PhoneNo').setValue('');
      this.filterForm.get('EmailId').setValue('');
      this.toastr.success('Created Successfully', 'User', {
        timeOut: 3000
    });

    })
  }
  rolesFetch(){

    this.UsersCreateService.getRoles().subscribe(data=>{
      // console.log(data)
      this.roles=data
    })
  }
  open(content) {
    // this.deleteId=id;
    // this.deleteThought(id);
    this.rolesFetch();

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
