export interface UserList {
    userId: any,
    username: any,
    password: any,
    roleId: any,
    firstName: any,
    lastName: any,
    phoneNo: any,
    emailId: any,
    isActive: true,
    createdOn: any,
    updatedOn: any,
    role: any
}