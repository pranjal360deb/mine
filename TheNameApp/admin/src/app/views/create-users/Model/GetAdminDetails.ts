export interface GetAdminDetails{
    userId: any,
    username: any,
    password: any,
    roleId: number,
    firstName: any,
    lastName: any,
    phoneNo: any,
    emailId: any,
    isActive: any,
    createdOn: any,
    updatedOn: any,
    role: any
}