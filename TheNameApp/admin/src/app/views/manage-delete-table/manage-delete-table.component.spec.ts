import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageDeleteTableComponent } from './manage-delete-table.component';

describe('ManageDeleteTableComponent', () => {
  let component: ManageDeleteTableComponent;
  let fixture: ComponentFixture<ManageDeleteTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageDeleteTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageDeleteTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
