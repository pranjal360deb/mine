import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError, finalize, filter, take, switchMap,mapTo, tap } from "rxjs/operators";
import { Observable, throwError } from 'rxjs';
import { DeleteReason } from '../model/delete-reason';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ManageDeleteService {

  constructor(private http: HttpClient) { }
  public URL: any =environment.baseURL+"/api/Reason";

  getDeleteReason() :Observable<DeleteReason[]>{
    let mainURL =this.URL;
    return this.http.get<DeleteReason[]>(mainURL);
  }
  postDeleteReason(Reason:string, Description:string) :Observable<any>{
    let mainURL =this.URL+"?Reason="+Reason+"&Description="+Description;
    return this.http.post<any>(mainURL,Reason);
  }

  UpdateDeleteReason(reasonId,Reason?:string, Description?:string) :Observable<any>{
    let mainURL =this.URL+"/"+reasonId+"?Reason="+Reason+"&Description="+Description;
    return this.http.put<any>(mainURL,Reason);
  }

  DeleteReason(reasonId) :Observable<any>{
    let mainURL =this.URL+"/"+reasonId
    return this.http.delete<any>(mainURL);
  }
}
