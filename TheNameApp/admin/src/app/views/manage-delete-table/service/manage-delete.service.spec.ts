import { TestBed } from '@angular/core/testing';

import { ManageDeleteService } from './manage-delete.service';

describe('ManageDeleteService', () => {
  let service: ManageDeleteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManageDeleteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
