import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {ManageDeleteService} from './service/manage-delete.service'
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manage-delete-table',
  templateUrl: './manage-delete-table.component.html',
  styleUrls: ['./manage-delete-table.component.scss']
})
export class ManageDeleteTableComponent implements OnInit {
  deleteDetails=[]
  myControl = new FormControl();
  filterForm: FormGroup;
  closeResult = '';
  reason:any;
  reasonId:any
  deleteId:any
  description:any;
  constructor(private toastr: ToastrService,private modalService: NgbModal,private formBuilder: FormBuilder,public ManageDeleteService:ManageDeleteService) { }
  

  ngOnInit(): void {
    this.getDeletes()
    this.filterForm = this.formBuilder.group({
      Reason: '',
      // ToDate: [''],
      // reported: [],
      Description:'',
      // corelationId:'',    
    });
  }

  getDeletes(){
      this.ManageDeleteService.getDeleteReason().subscribe(data=>{
        this.deleteDetails=data;
      })
  
  }
  get f() { return this.filterForm.controls; }

  filter(){
    this.reason = this.f.Reason.value;
    // this.repor = rep;
    this.description = this.f.Description.value; 
    // console.log("this is reason"+this.repor);
    // console.log("this is reason"+fd);
    this.updateReson();
  }
  updateReson(){
    this.ManageDeleteService.UpdateDeleteReason(this.reasonId,this.reason,this.description).subscribe(data=>{

      // console.log(data);
      this.modalService.dismissAll();
      this.toastr.success('Update Successfully', 'Your Content', {
        timeOut: 3000
    });
    })
  }

  deleting(){
    this.ManageDeleteService.DeleteReason(this.deleteId).subscribe(data=>{
      // console.log(data);
      this.modalService.dismissAll();
      this.toastr.success('Delete Successfully', 'Your Content', {
        timeOut: 3000
    });
    })
  }

  open(content,reasonID,rsn,descip) {
    this.reasonId=reasonID;
    // this.deleteThought(id);
    // console.log( this.reasonId);
    this.filterForm.get('Reason').setValue(rsn);
    this.filterForm.get('Description').setValue(descip);


    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
 

  delete(contenttt,reasonID) {
    this.deleteId=reasonID;
    // this.deleteThought(id);
    // console.log( this.reasonId);

    this.modalService.open(contenttt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonl(reason)}`;
    });
   
  }

  private getDismissReasonl(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
