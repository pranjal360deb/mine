import { Component, OnInit } from '@angular/core';
import { ThoughtService } from './thought.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import {ContentTypeDetails} from '../Model/contentEnum'
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { DashService } from '../dashboard/dash.service';
import{ManageDeleteService} from '../manage-delete-table/service/manage-delete.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-thought',
  templateUrl: './thought.component.html',
  styleUrls: ['./thought.component.scss']
})
export class ThoughtComponent implements OnInit {
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]>;
  deleteForm: FormGroup;

  filterForm: FormGroup;
  fromdate:any;
  fromday:any;
  frommonth:any;
  fromyear:any;
  today:any;
  tomonth:any;
  toyear:any;
  todate:string;
  openModal:boolean = false;
  repor:boolean = false;
  page: number = 0;
deleteId:number;
  closeResult = '';

  public userDetails =[]
  isLoading:boolean=false;
  num:number;
  coID:any;
  reported = [
      { id: 1, name: 'Reported' },
      { id: 2, name: 'Deleted' },
      { id: 3, name: 'Verified' },
  ];
  public userList =[]


  ImageCount:number;
  ImageReportedCount:number;
  ThoughtReportedCount:number;
  NoteReportedCount:number;
  AskReportedCount:number;
  ArticleReportedCount:number;
  StoryReportedCount:number;
  MusiceReportedCount:number;
  PodcastReportedCount:number;
  videoReportedCount:number;



  videoCount:number;
  PodcastCount:number;
  MusicCount:number;
  StoryCount:number;
  ArticleCount:number;
  AskCount:number;
  NoteCount:number;
  ThoughtCount:number;
  feedbackCounting:number;
  ideaCounting:number;
  bugCounting:number;

  pageNo = [
   25,
   50,
   75,
   100
];
public contentDetails = [];
pages=[]
pagenum:any;
  pagsize:any;
  i:any;
  pagingFilter:number=0;
  pagesall = 1;
  collectionSize =100;
  pagggee = 25

  IsReported: boolean = false;
  Isverified:boolean = false;
  IsDeleted:boolean = false;

  deleting;
reasonForDelete;
allreasons=[]
nameValue=''
contentId:any;
  reportListUser=[];
  isdisable: boolean=false;
  isRefresh:boolean=false;
  constructor(private toastr: ToastrService,private ManageDeleteService: ManageDeleteService,private dashservice: DashService, private datePipe: DatePipe,private formBuilder: FormBuilder,public userDetail: ThoughtService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getContentType();
    // this.default();
    this.getThought()
    this.filterForm = this.formBuilder.group({
      FromDate: '',
      ToDate: '',
      reported: [],
      name:'',
      corelationId:'',
      IsReported: false,
      Isverified:false,
      IsDeleted:false,
    });
    this.deleteForm = this.formBuilder.group({
      // FromDate: '',
      // ToDate: '',
      // reported: [],
      // name:'',
      // corelationId:'',
      reasonId:0,
      // delete:false
    });
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }
  refresh(){
    this.isRefresh=true;
    this.userDetails=[];
    this.isLoading=false;
    this.getThought();
  }
  reason(){
    this.ManageDeleteService.getDeleteReason().subscribe(data=>{
      this.allreasons=data
    })
  }
  deleteConfirm(){
    // this.deleting=this.dele.delete.value;
    
    this.reasonForDelete=this.dele.reasonId.value;
    this.delete();
  }
  get dele() { return this.deleteForm.controls; }



  loadPage(pageNumber: number) {
    // if (page !== this.previousPage) {
    //   this.previousPage = page;
    //   this.loadData();
    this.isLoading=false;
    this.userDetails=[];
    this.page = pageNumber - 1;
    // }
    this.getThought()
    // console.log(this.page);
  }

  getContentType(){
    this.dashservice.getDetails().subscribe(data=>{
      this.contentDetails=data;
      this.contentDetails.forEach(contentDetail => {

        switch(contentDetail.contentType){
          case ContentTypeDetails.Thought: 
          {
            this.videoCount = contentDetail.totalCount;
            this.videoReportedCount = contentDetail.reportedCount;
            break;
          }

        }
        
      });
    })
  }
  default(){
    this.pages=[]
    this.getContentType()
    this.pagsize =25;
    this.pagenum = this.videoCount/this.pagsize;
    // console.log(this.pagenum);
    this.pageList();
  }
  next(paging){
    this.pagingFilter=paging
    this.pagination()
    // console.log("page number is"+page);
  }
  pageList(){
    for(this.i=0;this.i<=this.pagenum;this.i++){
      this.pages.push(this.i)
    }
  }
  pagination(){
    this.userDetail.getPage(ContentTypeDetails.Thought,this.pagingFilter,this.pagsize).subscribe(data=>{
      this.userDetails=data;
          // console.log(data);
    })
  }
  onOptionsSelected(event){
    this.pagggee=0
    this.pagggee = event.target.value;
    this.page=0

  //   this.pages=[]
  //   this.getContentType()
  //   this.pagsize = event.target.value;
  //   this.pagenum = this.videoCount/parseInt(this.pagsize);


  //   let num = parseInt(this.pagenum)
    
  //   this.pageList();
   this.getThought()
    // console.log(value);
 
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  selectUser(id,name){

    this.filterForm.get('corelationId').setValue(id);
    this.filterForm.get('name').setValue(name);

  }
  onSearchChange(searchValue: string): void {  
    // console.log(searchValue);
    this.userDetail.getUserDetails(searchValue).subscribe(data=>{
      this.userList=data
      // console.log(data);
    })
  }
  get f() { return this.filterForm.controls; }

  filter(){
    this.isLoading=false;
    let rep = this.f.reported.value;
    this.repor = rep;
    let fd = this.f.FromDate.value;
    this.num = rep;
    this.coID = this.f.corelationId.value;
    // let date =this.datePipe.transform(fd,'yyyy-dd-MM');
    this.fromdate = this.datePipe.transform(fd,'yyyy-MM-dd');

    this.IsReported = this.f.IsReported.value;
    this.Isverified = this.f.Isverified.value;
    this.IsDeleted= this.f.IsDeleted.value;

    // this.today=this.f.ToDate.value.day;
    // this.tomonth=this.f.ToDate.value.month;
    // this.toyear=this.f.ToDate.value.year;

    // let tday = this.today.toString();
    // let tmonth = this.tomonth.toString();
    // let tyear = this.toyear.toString();
    // let tDate = tyear + "-" + tmonth + "-" + tday;
    let td = this.f.ToDate.value;
    this.todate=this.datePipe.transform(td,'yyyy-MM-dd');;

    this.userDetails=[];
    this.page=0;

  // this.userDetail.getUser(9,0,this.fromdate,this.todate,this.repor).subscribe(data=>{
  //     this.userDetails=data;
  //   })
    this.getThought();

  }

  delete(){
    // console.log(this.deleteId);
    // this.userDetail.deleteThought(ContentTypeDetails.Thought,this.deleteId).subscribe(data=>{
    //   // console.log('susscefully deleted'+data);
    //   this.modalService.dismissAll();

    // })
    this.userDetail.deletingContent(ContentTypeDetails.Thought, this.deleteId,this.deleting,this.reasonForDelete).subscribe(data=>{
      // console.log(data);
      this.modalService.dismissAll()
      this.toastr.success(this.nameValue+' Successfully', 'Your Content', {
        timeOut: 3000
    });
    })
    // this.openModal=true;
  }

  verifying(){
    this.userDetail.UpdateContent(ContentTypeDetails.Thought,this.deleteId).subscribe(data=>{
      // console.log('susscefully deleted'+data);
      // this.openModal=true;
      this.modalService.dismissAll();
      this.toastr.success('Verify Successfully', 'Your Content', {
        timeOut: 3000
    });
    })

  }
deleteThought(id){
console.log(id);
}
getThought(){
//   if(this.page==0)
//  { 
   this.userDetail.getUser(ContentTypeDetails.Thought,this.page,this.IsReported,this.Isverified,this.IsDeleted,this.fromdate,this.todate,this.coID,this.pagggee).subscribe(data=>{
    this.isLoading=true;
    this.isRefresh=false;
    // data.forEach(element => {
    //   // console.log(element.reasonId);
    //   if(element.reasonId=null){
    //     this.isdisable=true;

    //   }
    //   else{
    //     this.isdisable=false;

    //   }
    // });
    this.userDetails=data;
  })
// }
//   else{
//     this.userDetail.getUser(ContentTypeDetails.Thought,this.page,this.fromdate,this.todate,this.repor,this.coID).subscribe(data=>{
//       this.userDetails= this.userDetails.concat(data);
//     })

//   }
}
onScrolldown() {
  this.page = this.page + 1;
  this.getThought();
}
  open(content,id,boolValue,name,value?:any) {
    // this.deleteThought(id);
    this.reason()
    this.deleteId=id;
    this.deleting = boolValue;
    this.nameValue=name
    console.log(value);
    this.deleteForm.get('reasonId').setValue(value);
    // this.deleteThought(id);
    // this.deleteForm.get('delete').setValue(del);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  verify(contentt,id) {
    this.deleteId=id;
    // this.deleteThought(id);

    this.modalService.open(contentt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonal(reason)}`;
    });
   
  }
  private getDismissReasonal(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  reportContent(){
    this.userDetail.report(ContentTypeDetails.Thought,this.contentId).subscribe(data=>{
      // console.log(data)
      this.reportListUser=data
    });
  }
  reportUser(reportUserList,id) {
    this.contentId=id;
    this.reportContent();
    // this.deleteId=id;
    // this.reason();
    // this.deleting = boolValue;
    // this.nameValue=name
    // console.log(boolValue);
    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(reportUserList, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonReported(reason)}`;
    });
   
  }
  private getDismissReasonReported(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
