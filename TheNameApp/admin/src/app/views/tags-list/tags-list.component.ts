import { Component, OnInit } from '@angular/core';
import {AllTagsService} from '../tags/services/all-tags.service'
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ContentTypeDetails} from '../Model/contentEnum'
@Component({
  selector: 'app-tags-list',
  templateUrl: './tags-list.component.html',
  styleUrls: ['./tags-list.component.scss']
})
export class TagsListComponent implements OnInit {
  tags=[]
  closeResult=''
  tagName=''
  type;
  pageNo=0 ;
  allTypeContent=[]
  serachTag=''
  pageSize=25;
  tagCount=100

  pageNos = [
    25,
    50,
    75,
    100
 ];

 tagpageNos = [
  25,
  50,
  75,
  100
];
 pagesall = 1;
 pageSizetag=25;
 nameTag
  constructor(private modalService:NgbModal, private AllTagsService: AllTagsService) { }

  ngOnInit(): void {
    this.allTags();
  }

  loadPage(pageNumber: number) {
    // if (page !== this.previousPage) {
    //   this.previousPage = page;
    //   this.loadData();
    // this.isLoading=false;
    this.allTypeContent=[];
    this.pageNo = pageNumber - 1;
    // }
    this.tagContent();    // console.log(this.page);
  }
  ontagPageSelected(event){
    this.pageSize=0
    this.pageSizetag = event.target.value;
    this.pageNo=0
  //   this.pages=[]
  //   this.getContentType()
  //   this.pagsize = event.target.value;
  //   this.pagenum = this.videoCount/parseInt(this.pagsize);


  //   let num = parseInt(this.pagenum)
    
  //   this.pageList();?
   this.tagContent();
    // console.log(value);
 
  }
  onOptionsSelected(event){
    this.pageSize=0
    this.pageSize = event.target.value;
    this.pageNo=0
  //   this.pages=[]
  //   this.getContentType()
  //   this.pagsize = event.target.value;
  //   this.pagenum = this.videoCount/parseInt(this.pagsize);


  //   let num = parseInt(this.pagenum)
    
  //   this.pageList();
   this.allTags();
    // console.log(value);
 
  }
  allTags(){
    this.AllTagsService.getAllTags(this.serachTag,this.pageNo,this.pageSize).subscribe(data=>{
      // console.log(data);
      this.tags=data;
      // console.log(this.tags.length)
    })
  }
  onSearchChange(value){

    this.serachTag=value;
    // console.log(value);
    this.allTags();
  }

  tagContent(){
    this.AllTagsService.getContentTags(this.pageNo,this.type,this.tagName,this.pageSizetag).subscribe(data=>{  

      this.allTypeContent=data
      // console.log(this.allTypeContent.mediaIds)
      // this.allTypeContent.forEach(element => {
      //   // element.mediaIds.forEach(element1 => {
      //   //   console.log(element1.blobId);
      //   // });
      //   console.log(element.mediaIds);

      // });
    })
  }
  
  open(content,imageNmae,imgcount,tag) {
    // this.deleteId=id;
    this.nameTag=tag
    this.tagCount=imgcount;
    this.allTypeContent=[];
    this.tagName=imageNmae;
    this.type=ContentTypeDetails.Image;
    // console.log(imageNmae);   
    this.tagContent()
    // this.deleteThought(id);

    this.modalService.open(content, {windowClass : "tags",ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  video(content,imageNmae,imgcount,tag) {
    // this.deleteId=id;\\
    this.nameTag=tag

    this.tagCount=0;
    this.tagCount=imgcount;

    this.allTypeContent=[];
    this.tagName=imageNmae;
    this.type=ContentTypeDetails.Video;
    // console.log(imageNmae);   
    this.tagContent()
    // this.deleteThought(id);

    this.modalService.open(content, {windowClass : "tags",ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }
   stories(content,imageNmae,imgcount,tag) {
    this.nameTag=tag
    this.tagCount=0;
    this.tagCount=imgcount;
    this.allTypeContent=[];
    this.tagName=imageNmae;
    this.type=ContentTypeDetails.Story;
    // console.log(imageNmae);   
    this.tagContent()
    // this.deleteThought(id);

    this.modalService.open(content, {windowClass : "tags",ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }
  thoughts(content,imageNmae,imgcount,tag) {
    this.nameTag=tag
    this.tagCount=0;
    this.tagCount=imgcount;
    this.allTypeContent=[];
    this.tagName=imageNmae;
    this.type=ContentTypeDetails.Thought;
    // console.log(imageNmae);   
    this.tagContent()
    // this.deleteThought(id);

    this.modalService.open(content, {windowClass : "tags",ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }
  notes(content,imageNmae,imgcount,tag) {
    this.nameTag=tag
    this.tagCount=0;
    this.tagCount=imgcount;
    this.allTypeContent=[];
    this.tagName=imageNmae;
    this.type=ContentTypeDetails.Note;
    // console.log(imageNmae);   
    this.tagContent()
    // this.deleteThought(id);

    this.modalService.open(content, {windowClass : "tags",ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  podcast(content,imageNmae,imgcount,tag) {
    this.nameTag=tag
    this.tagCount=0;
    this.tagCount=imgcount;
    this.allTypeContent=[];
    this.tagName=imageNmae;
    this.type=ContentTypeDetails.Podcast;
    // console.log(imageNmae);   
    this.tagContent()
    // this.deleteThought(id);

    this.modalService.open(content, {windowClass : "tags",ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  music(content,imageNmae,imgcount,tag) {
    this.nameTag=tag
    this.tagCount=0;
    this.tagCount=imgcount;
    this.allTypeContent=[];
    this.tagName=imageNmae;
    this.type=ContentTypeDetails.Music;
    // console.log(imageNmae);   
    this.tagContent()
    // this.deleteThought(id);

    this.modalService.open(content, {windowClass : "tags",ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  article(content,imageNmae,imgcount,tag) {
    this.nameTag=tag
    this.tagCount=0;
    this.tagCount=imgcount;
    this.allTypeContent=[];
    this.tagName=imageNmae;
    this.type=ContentTypeDetails.Article;
    // console.log(imageNmae);   
    this.tagContent()
    // this.deleteThought(id);

    this.modalService.open(content, {windowClass : "tags",ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  ask(content,imageNmae,imgcount,tag) {
    this.nameTag=tag
    this.tagCount=0;
    this.tagCount=imgcount;
    this.allTypeContent=[];
    this.tagName=imageNmae;
    this.type=ContentTypeDetails.Ask;
    // console.log(imageNmae);   
    this.tagContent()
    // this.deleteThought(id);

    this.modalService.open(content, {windowClass : "tags",ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

}
