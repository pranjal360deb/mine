import { Component, OnInit } from '@angular/core';
import {UsersCreateService} from '../create-users/services/users-create.service'
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  filterForm: FormGroup;
  AdminId;
pageNo=0
deleteId:any;
listUser=[];
closeResult='';
roles=[]
pageNos = [
  25,
  50,
  75,
  100
];
count
pagesall = 1;
pagggee = 25
userName;
  passwd;
  roleID;
  fName;
  lName;
  phone;
  email;
  UserID:any;
  submitted = false;
  active
  
  constructor(private UsersCreateService:UsersCreateService,private modalService: NgbModal,private toastr: ToastrService,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.adminCount();
    this.listOfUser();
    this.filterForm = this.formBuilder.group({
      Username:['',[Validators.required]],
      Password:['',[Validators.minLength(6)]],
      RoleId:['',[Validators.required]],
      FirstName:['',[Validators.required]],
      LastName:['',[Validators.required]],
      PhoneNo:['',[Validators.required]],
      EmailId:['',[Validators.required]], 
      IsActive: true
    });
  }
  adminCount(){
    this.UsersCreateService.getCount().subscribe(data=>{
      this.count=data
      // console.log(data)
    })
  }
  loadPage(pageNumber: number) {
    // if (page !== this.previousPage) {
    //   this.previousPage = page;
    //   this.loadData();
    // this.isLoading=false;
    this.listUser=[];
    this.pageNo = pageNumber - 1;
    // }
    this.listOfUser()
    // console.log(this.page);
  }
  onOptionsSelected(event){
    this.pagggee=0
    this.pagggee = event.target.value;
    this.pageNo=0
  //   this.pages=[]
  //   this.getContentType()
  //   this.pagsize = event.target.value;
  //   this.pagenum = this.videoCount/parseInt(this.pagsize);
  //   let num = parseInt(this.pagenum)
    
  //   this.pageList();
   this.listOfUser()
    // console.log(value);
 
  }
  get f() { return this.filterForm.controls; }
  filter(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.filterForm.invalid) {
        return;
    }else
   { 
    this.userName = this.f.Username.value;
    this.passwd = this.f.Password.value;
    this.roleID = this.f.RoleId.value;
    this.fName = this.f.FirstName.value;
    this.lName = this.f.LastName.value;
    this.phone = this.f.PhoneNo.value;
    this.email = this.f.EmailId.value;
    this.active = this.f.IsActive.value;
    this.updatingUsers();
    // console.log(this.userName,this.passwd,this.roleID,this.fName, this.lName, this.phone,this.email)
}
    // this.repor = rep;
    // this.description = this.f.Description.value; 
    // console.log("this is reason"+this.repor);
    // console.log("this is reason"+fd);
    // this.post()
  }

  updatingUsers(){
    this.UsersCreateService.updateUser(this.AdminId,this.UserID,this.active,this.userName,this.roleID,this.fName,this.lName,this.phone,this.email,this.passwd).subscribe(data=>{
      // console.log(data)
      this.modalService.dismissAll()
      this.toastr.success('Update Successfully', 'User', {
        timeOut: 3000
    });

    })
  }
  listOfUser(){
this.UsersCreateService.getUserList(this.pageNo,this.pagggee).subscribe(data=>{
this.listUser=data
})
  }
  rolesFetch(){

    this.UsersCreateService.getRoles().subscribe(data=>{
      // console.log(data)
      this.roles=data
    })
  }
  deleting(){
    this.UsersCreateService.Delete(this.deleteId).subscribe(data=>{
      this.modalService.dismissAll()
      this.toastr.success('Delete Successfully', 'User', {
        timeOut: 3000
    });
    })

  }
  delete(contenttt,reasonID) {
    this.deleteId=reasonID;
    // this.deleteThought(id);
    // console.log( this.reasonId);

    this.modalService.open(contenttt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonl(reason)}`;
    });
   
  }

  private getDismissReasonl(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  open(content,userId,roleId,userName,firstName,lastName,pNumber,email,active) {
    this.UserID=userId
    this.AdminId=userId
    this.rolesFetch();
    this.filterForm.get('RoleId').setValue(roleId);
    this.filterForm.get('Username').setValue(userName);
    this.filterForm.get('FirstName').setValue(firstName);
    this.filterForm.get('LastName').setValue(lastName);
    this.filterForm.get('PhoneNo').setValue(pNumber);
    this.filterForm.get('EmailId').setValue(email);
    this.filterForm.get('IsActive').setValue(active);

    // this.messageId=customMessageId;
    // this.f.message=msg
    // this.filterForm.get('message').setValue(msg);
    // this.deleteThought(id);
    // console.log( this.reasonId);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
