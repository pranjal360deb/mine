import { Component, OnInit, Input } from '@angular/core';
import { ContentTypeDetails } from '../Model/contentEnum';
import {ThoughtService} from '../thought/thought.service'
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import { Observable } from 'rxjs';
import{ManageDeleteService} from '../manage-delete-table/service/manage-delete.service'
import { NgbModal, ModalDismissReasons,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-thought-card',
  templateUrl: './thought-card.component.html',
  styleUrls: ['./thought-card.component.scss']
})
export class ThoughtCardComponent implements OnInit {

  @Input() thoughtDetails:any;
  allThought:any;
  deleteForm: FormGroup;
  isdisable:any;
  closeResult=''
  reportListUser=[]
  deleteId:any
  deleting:any
  nameValue:any
  contentId:any
  reasonForDelete:any
  allreasons=[]
  textfeild=''
  constructor(private ThoughtService:ThoughtService,private ManageDeleteService: ManageDeleteService,private formBuilder: FormBuilder,private toastr: ToastrService,private modalService: NgbModal) { }

  ngOnInit(): void {
    if(this.thoughtDetails.reasonId==null) {
      this.isdisable= true
    }
    else{
      this.isdisable= false
    }
    this.allThought=this.thoughtDetails
    this.deleteForm = this.formBuilder.group({
      // FromDate: '',
      // ToDate: '',
      // reported: [],
      // name:'',
      // corelationId:'',
      reasonId:0,
      // delete:false
    });
  }
  openThoughts(content,anyOtherViews) {
    this.textfeild=anyOtherViews;
    // this.titlefeild=title;

    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getTHoughtDismissReason(reason)}`;
    });
   
  }

  private getTHoughtDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  delete(){
    // console.log(this.deleteId);
    // this.userDetail.deleteThought(ContentTypeDetails.Thought,this.deleteId).subscribe(data=>{
    //   // console.log('susscefully deleted'+data);
    //   this.modalService.dismissAll();

    // })
    this.ThoughtService.deletingContent(ContentTypeDetails.Thought, this.deleteId,this.deleting,this.reasonForDelete).subscribe(data=>{
      // console.log(data);
      this.modalService.dismissAll()
      this.toastr.success(this.nameValue+' Successfully', 'Your Content', {
        timeOut: 3000
    });
    })
    // this.openModal=true;
  }

  verifying(){
    this.ThoughtService.UpdateContent(ContentTypeDetails.Thought,this.deleteId).subscribe(data=>{
      // console.log('susscefully deleted'+data);
      // this.openModal=true;
      this.modalService.dismissAll();
      this.toastr.success('Verify Successfully', 'Your Content', {
        timeOut: 3000
    });
    })

  }
  reason(){
    this.ManageDeleteService.getDeleteReason().subscribe(data=>{
      this.allreasons=data
    })
  }  
  deleteConfirm(){
    // this.deleting=this.dele.delete.value;
    
    this.reasonForDelete=this.dele.reasonId.value;
    this.delete();
  }
  get dele() { return this.deleteForm.controls; }

  open(content,id,boolValue,name) {
    // this.deleteThought(id);
    this.reason()
    this.deleteId=id;
    this.deleting = boolValue;
    this.nameValue=name
    // console.log(value);
    // this.deleteForm.get('reasonId').setValue(value);
    // this.deleteThought(id);
    // this.deleteForm.get('delete').setValue(del);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  deleteopen(content,id,boolValue,deleteValue,value?:any) {
    this.reason()
    this.deleteId=id;
    this.deleting = boolValue;
    this.nameValue=deleteValue;
    console.log(value);
    this.deleteForm.get('reasonId').setValue(value);
    // this.deleteAskForm.get('delete').setValue(del);
    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getdeleteDismissReason(reason)}`;
    });
   
  }

  private getdeleteDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  verify(contentt,id) {
    this.deleteId=id;
    // this.deleteThought(id);

    this.modalService.open(contentt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonal(reason)}`;
    });
   
  }
  private getDismissReasonal(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  reportContent(){
    this.ThoughtService.report(ContentTypeDetails.Thought,this.contentId).subscribe(data=>{
      // console.log(data)
      this.reportListUser=data
    });
  }
  reportUser(reportUserList,id) {
    this.contentId=id;
    this.reportContent();
    // this.deleteId=id;
    // this.reason();
    // this.deleting = boolValue;
    // this.nameValue=name
    // console.log(boolValue);
    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(reportUserList, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonReported(reason)}`;
    });
   
  }
  private getDismissReasonReported(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
