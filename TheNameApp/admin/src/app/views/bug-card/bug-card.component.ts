import { Component, OnInit, Input } from '@angular/core';
import { SwiperOptions } from 'swiper';
import { NgbModal, ModalDismissReasons,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-bug-card',
  templateUrl: './bug-card.component.html',
  styleUrls: ['./bug-card.component.scss']
})
export class BugCardComponent implements OnInit {
@Input() bugs;
bug:any
imageString:any ;
blobIDs:any;
textfeild=""
closeResult=''
img =[
  
];
im=[
]
  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
    this.bug=this.bugs
    this.blob();
  }
  blob(){
    this.bugs.medias.forEach(element => {
      // console.log(element.blobId);
      this.blobIDs= element;
      this.imageString=environment.baseURL+ "/BlobFile/"+this.blobIDs.blobId+"?ContentType="+this.blobIDs.contentType;
      // console.log(this.blobIDs.blobId);
      // console.log(this.blobIDs.contentType);
      // this.blobIDs=this.blobIDs.concat(element.contentType);
      // this.PhotoService.getBlobFile(this.blobIDs.blobId,this.blobIDs.contentType).subscribe(data=>{
      //   // console.log(data);
      //   // let objectURL = URL.createObjectURL(data);       
      //   // this.imageString = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        
      //   this.imageString=data;
      // })
      this.img.push(this.imageString);
      // let feed ={
      //   value:this.imageString,
      //   id:this.blobIDs.blobId
      // }
      // this.im.push(feed);

      

    }
    );
    
    // console.log(this.im)

  }

  opening(content,anyOtherViews) {
    this.textfeild=anyOtherViews;
    // this.titlefeild=title;

    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
