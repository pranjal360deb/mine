import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {CustomAlertService} from '../custom-alert-table/service/custom-alert.service'
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-custom-alert',
  templateUrl: './custom-alert.component.html',
  styleUrls: ['./custom-alert.component.scss']
})
export class CustomAlertComponent implements OnInit {
  filterForm: FormGroup;
  myControl = new FormControl();
  Message:any
  description:any;
  closeResult = '';
  AdminId=1;
  AlertType=0
  constructor(private toastr: ToastrService,private CustomAlertService: CustomAlertService, private modalService: NgbModal,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.filterForm = this.formBuilder.group({
      message: '',
      // ToDate: [''],
      // reported: [],
      // Description:'',
      // corelationId:'',    
    });
  }


  
  get f() { return this.filterForm.controls; }

  filter(){
    this.Message = this.f.message.value;
    // this.repor = rep;
    // this.description = this.f.Description.value; 
    // console.log("this is reason"+this.repor);
    // console.log("this is reason"+fd);
    this.post()
  }
  post(){

    this.CustomAlertService.postMessage(this.Message,this.AlertType,this.AdminId).subscribe(data=>{

      this.modalService.dismissAll();
      this.toastr.success('Submit Successfully', 'Your Alert', {
        timeOut: 3000
    });
    this.filterForm.get('message').setValue('');

    })
  }

  open(content) {
    // this.deleteId=id;
    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
