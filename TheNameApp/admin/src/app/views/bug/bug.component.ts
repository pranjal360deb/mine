import { Component, OnInit } from '@angular/core';
import { ContentFetchService } from '../services/content-fetch.service';
import { DashService } from '../dashboard/dash.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-bug',
  templateUrl: './bug.component.html',
  styleUrls: ['./bug.component.scss']
})
export class BugComponent implements OnInit {
  public bugDetails = []
  page: number = 0;
  filterForm: FormGroup;
  statusForm: FormGroup;
  fileUrl;
  pagesall = 1;
  collectionSize = 100;
  pagggee = 25

  pageNo = [
    25,
    50,
    75,
    100
  ];
  status = [
   
  ];
  bugSat:any=1;
  coID:any
  fromdate:any
  todate:any
  bugCounting: number;
  userList = [];
  blobIDs: any;
  img = [];
  im = []
  imageString:any ;
  media:any;
  closeResult = '';
  bugID:any
  updateStatus:any
  isLoading:boolean=false;
  isRefresh:boolean=false;
  // IsReported:boolean= false;
  constructor(private modalService: NgbModal,private datePipe: DatePipe,private sanitizer: DomSanitizer, private formBuilder: FormBuilder, private dashservice: DashService, public ContentFetchService: ContentFetchService) { }

  ngOnInit(): void {
    this.getBugStatus()
    this.getBug();
    this.getBugcount();
    // this.blobImage()
    this.filterForm = this.formBuilder.group({
      FromDate: '',
      ToDate: '',
      reported: [],
      name: '',
      corelationId: '',
      // IsReported: true,
      // Isverified: false,
      // IsDeleted: false,
      bugStatus:this.bugSat
    });
    this.statusForm = this.formBuilder.group({
      // FromDate: '',
      // ToDate: '',
      // reported: [],
      // name: '',
      // corelationId: '',
      // IsReported: false,
      // Isverified: false,
      // IsDeleted: false,
      bugStatus:this.bugSat
    });
    // const data = 'hi';
    // const blob = new Blob(['https://wallpapercave.com/wp/wp1885553.jpg'], { type: 'application/octet-stream' });

    // this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));

  }
  blobImage() {
   


    this.bugDetails.forEach(element => {
      element.medias.forEach(ele => {
        this.blobIDs = ele;
          this.imageString = environment.baseURL+"/BlobFile/" + this.blobIDs.blobId + "?ContentType=" + this.blobIDs.contentType;
          this.img.push(this.imageString);
        
      });
    });
    // console.log(this.img)

  }

  getBugStatus(){
    this.ContentFetchService.getBugStatus().subscribe(data=>{

      this.status=data;
      // console.log(this.status)
      data.forEach(element => {
        if(element.bugStatusId==1)
        {
          this.bugSat=element.bugStatusId
          // console.log(element.bugStatusId)
        }
      });
    })
  }
  // FieldsChange(values:any){
  //   console.log(values.currentTarget.checked);
  //   }

  get st() { return this.statusForm.controls; }

  bugStatusUpdate(){
    this.updateStatus=this.st.bugStatus.value;
    this.updateStatusBug()
  }
  updateStatusBug(){
    this.ContentFetchService.UpdateBugStatus(this.bugID,this.updateStatus).subscribe(data=>{
      // console.log(data);
      this.modalService.dismissAll();
    })
  }


  selectUser(id, name) {

    this.filterForm.get('corelationId').setValue(id);
    this.filterForm.get('name').setValue(name);

  }
  onSearchChange(searchValue: string): void {
    // console.log(searchValue);
    this.ContentFetchService.getUser(searchValue).subscribe(data => {
      this.userList = data
      // console.log(data);
    })
  }
  get f() { return this.filterForm.controls; }

  filter() {
// this.IsReported=this.f.IsReported.value;
// console.log(this.IsReported);
this.isLoading=false
    this.coID = this.f.corelationId.value;
    let fd = this.f.FromDate.value;
    // let date =this.datePipe.transform(fd,'yyyy-dd-MM');
    this.fromdate = this.datePipe.transform(fd,'yyyy-MM-dd');
    let td = this.f.ToDate.value;
    this.todate=this.datePipe.transform(td,'yyyy-MM-dd');;

    this.bugSat=this.f.bugStatus.value;
    this.bugDetails=[];
    this.page=0;
    this.getBug();
    
  }
  refresh(){
    this.isRefresh=true;
    this.bugDetails=[];
    this.isLoading=false;
    this.getBug();
  }
  loadPage(pageNumber: number) {
    // if (page !== this.previousPage) {
    //   this.previousPage = page;
    //   this.loadData();
    this.isLoading=false;
    this.bugDetails=[];
    this.page = pageNumber - 1;
    // }
    this.getBug()
    // console.log(this.page);
  }
  getBug() {
    // if(this.page==0)
    // { 
    this.ContentFetchService.bugGet(this.page,this.fromdate,this.todate,this.bugSat,this.coID,this.pagggee).subscribe(data => {
      this.isLoading=true;
      this.isRefresh=false;
      this.bugDetails = data;
      
    })

    //  }
    //    else{
    //     this.ContentFetchService.feedbackGet(this.page).subscribe(data=>{
    //       this.userDetails=this.userDetails.concat(data);;
    //     })

    //    }
  }
  getBugcount() {
    this.dashservice.getBugCount().subscribe(data => {
      this.bugCounting = data;
    });
  }


  onOptionsSelected(event) {
    this.pagggee = 0
    this.pagggee = event.target.value;
    this.page=0
    //   this.pages=[]
    //   this.getContentType()
    //   this.pagsize = event.target.value;
    //   this.pagenum = this.videoCount/parseInt(this.pagsize);


    //   let num = parseInt(this.pagenum)

    //   this.pageList();
     this.getBug();
    // console.log(value);

  }


  open(content,id) {
    this.bugID=id;
    // console.log(this.bugID)
    // this.f.message=msg
    // this.filterForm.get('message').setValue(msg);
    // this.deleteThought(id);
    // console.log( this.reasonId);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
