import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ContentTypeDetails } from '../Model/contentEnum';
import {ContentFetchService} from '../services/content-fetch.service'
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import { Observable } from 'rxjs';
import{ManageDeleteService} from '../manage-delete-table/service/manage-delete.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'article-card',
  templateUrl: './article-card.component.html',
  styleUrls: ['./article-card.component.scss']
})
export class ArticleCardComponent implements OnInit {
  filterForm: FormGroup;
  filteredOptions: Observable<string[]>;
  myControl = new FormControl();

  @Input() articles;
  article:any;
  imageString:any;
  textfeild:any;
  titlefeild:any;
  closeResult = '';
  deleteId:number;
  isdisable: boolean=false;
  // activeModal: NgbActiveModal
  openModal:boolean =false;
deleted=[]
deleting;
reasonForDelete;
allreasons=[]
nameValue=''
contentId:any;
  reportListUser=[];
  constructor(private ManageDeleteService: ManageDeleteService,private formBuilder: FormBuilder,private toastr: ToastrService,private modalService: NgbModal, private ContentFetchService:ContentFetchService ) {
   }

  ngOnInit(): void {
    if(this.articles.reasonId==null) {
      this.isdisable= true
    }
    else{
      this.isdisable= false
    }
    this.article=this.articles;
    this.blob();

    this.filterForm = this.formBuilder.group({
      // FromDate: '',
      // ToDate: '',
      // reported: [],
      // name:'',
      // corelationId:'',
      reasonId:0,
      // delete:false
    });

  }
reason(){
  this.ManageDeleteService.getDeleteReason().subscribe(data=>{
    this.allreasons=data
  })
}
  filter(){
    // this.deleting=this.f.delete.value;
    this.reasonForDelete=this.f.reasonId.value;
    this.delete();
  }
  get f() { return this.filterForm.controls; }

  delete(){
    // this.ContentFetchService.deleteContent(ContentTypeDetails.Video,this.deleteId).subscribe(data=>{
    //   // this.deleted =data;
    //   // console.log(data);
    //   // data.forEach(element => {
    //   //   console.log(element.result)
    //   // });
    //   // console.log('successful');
    //   // this.activeModal.close();
    //   this.modalService.dismissAll();

    //   this.toastr.success('Delete Successfully', 'Your Content', { positionClass: 'toast-top-right' });

    
    // })
    this.ContentFetchService.deletingContent(ContentTypeDetails.Article, this.deleteId,this.deleting,this.reasonForDelete).subscribe(data=>{
      // console.log(data);
      this.modalService.dismissAll()
      this.toastr.success(this.nameValue+' Successfully', 'Your Content',{
        timeOut: 3000
    } );
    })
  }

  blob(){
    
    this.imageString= environment.baseURL+"/BlobFile/"+this.articles.thumbnail.blobId+"?ContentType="+this.articles.thumbnail.contentType;
  }
  opening(content,text,title) {
    this.textfeild=text;
    this.titlefeild=title;

    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  verifying(){
    this.ContentFetchService.UpdateContent(ContentTypeDetails.Article,this.deleteId).subscribe(data=>{
      // console.log('susscefully verified');
      this.modalService.dismissAll();

      this.toastr.success('Verify Successfully', 'Your Content',{
        timeOut: 3000
    } );


      // this.openModal=true;
    })

  }

  open(contenttt,title,boolValue,name,value?:any) {
    // this.textfeild=text;
    // this.titlefeild=title;
    this.reason()
    this.deleteId=title;
    this.deleting = boolValue;
    this.nameValue=name
    this.filterForm.get('reasonId').setValue(value);
    // this.filterForm.get('delete').setValue(del);
    // this.deleteThought(id);

    this.modalService.open(contenttt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  verify(contentt,id) {
    this.deleteId=id;
    // this.deleteThought(id);

    this.modalService.open(contentt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonal(reason)}`;
    });
   
  }
  private getDismissReasonal(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  reportContent(){
    this.ContentFetchService.report(ContentTypeDetails.Article,this.contentId).subscribe(data=>{
      // console.log(data)
      this.reportListUser=data
    });
  }
  reportUser(reportUserList,id) {
    this.contentId=id;
    this.reportContent();
    // this.deleteId=id;
    // this.reason();
    // this.deleting = boolValue;
    // this.nameValue=name
    // console.log(boolValue);
    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(reportUserList, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonReported(reason)}`;
    });
   
  }
  private getDismissReasonReported(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
