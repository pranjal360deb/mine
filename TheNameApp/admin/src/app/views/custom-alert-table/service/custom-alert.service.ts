import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CustomAlert } from '../model/custom-alert';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomAlertService {

  constructor(private http: HttpClient) { }
  public URL: any =environment.baseURL+"/api/CustomAlert";

  postMessage(Message:string,AlertType:any, AdminId:any) :Observable<any>{
    let mainURL =this.URL+"?Message="+Message+"&AlertType="+AlertType+"&AdminId="+AdminId;
    return this.http.post<any>(mainURL,Message);
  }

 getMessage(page:number) :Observable<CustomAlert[]>{
    let mainURL =this.URL+"/"+page;
    return this.http.get<CustomAlert[]>(mainURL);
  }

 updateMessage(alertId:number,Message) :Observable<any>{
    let mainURL =this.URL+"/"+alertId+"?Message="+Message;
    return this.http.put<any>(mainURL,Message);
  }

  DeleteReason(reasonId) :Observable<any>{
    let mainURL =this.URL+"/"+reasonId
    return this.http.delete<any>(mainURL);
  }
}
