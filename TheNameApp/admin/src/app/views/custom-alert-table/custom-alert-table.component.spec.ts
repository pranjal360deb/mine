import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomAlertTableComponent } from './custom-alert-table.component';

describe('CustomAlertTableComponent', () => {
  let component: CustomAlertTableComponent;
  let fixture: ComponentFixture<CustomAlertTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomAlertTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomAlertTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
