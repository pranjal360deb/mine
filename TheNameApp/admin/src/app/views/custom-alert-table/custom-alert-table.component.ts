import { Component, OnInit } from '@angular/core';
import {CustomAlertService} from './service/custom-alert.service'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-custom-alert-table',
  templateUrl: './custom-alert-table.component.html',
  styleUrls: ['./custom-alert-table.component.scss']
})
export class CustomAlertTableComponent implements OnInit {
  closeResult = '';
  myControl = new FormControl();
  filterForm: FormGroup;
  // msg="iugyiuhyeoirhweoihrf eoi"
messages=[];
Messaging:string;
messageId;
deleteId:number;
  pageNum=0;
  constructor(private toastr: ToastrService,private modalService: NgbModal,private formBuilder: FormBuilder,private CustomAlertService: CustomAlertService) { }

  ngOnInit(): void {
    this.getMessage();
    this.filterForm = this.formBuilder.group({
      message: '',
      // ToDate: [''],
      // reported: [],
      // Description:'',
      // corelationId:'',    
    });
  }

  get f() { return this.filterForm.controls; }

  filter(){
    this.Messaging = this.f.message.value;
    // this.repor = rep;
    // this.description = this.f.Description.value; 
    // console.log("this is reason"+this.repor);
    // console.log("this is reason"+fd);
    this.updateReson();
  }
  getMessage(){
    this.CustomAlertService.getMessage(this.pageNum).subscribe(data=>{

      this.messages=data
    })
  }
  updateReson(){
    this.CustomAlertService.updateMessage(this.messageId,this.Messaging).subscribe(data=>{
      // console.log(data);
      this.modalService.dismissAll();
      this.toastr.success('Update Successfully', 'Your Content', {
        timeOut: 3000
    });
    })
  }


  deleting(){
    this.CustomAlertService.DeleteReason(this.deleteId).subscribe(data=>{
      // console.log(data);
      this.modalService.dismissAll();
      this.toastr.success('Delete Successfully', 'Your Content', {
        timeOut: 3000
    });
    })
  }
  open(content,customMessageId,msg) {
    this.messageId=customMessageId;
    // this.f.message=msg
    this.filterForm.get('message').setValue(msg);
    // this.deleteThought(id);
    // console.log( this.reasonId);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  delete(contenttt,reasonID) {
    this.deleteId=reasonID;
    // this.deleteThought(id);
    // console.log( this.reasonId);

    this.modalService.open(contenttt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonl(reason)}`;
    });
   
  }

  private getDismissReasonl(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
