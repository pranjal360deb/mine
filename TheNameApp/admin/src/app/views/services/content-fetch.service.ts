import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { map, catchError, finalize, filter, take, switchMap,mapTo, tap } from "rxjs/operators";
import { Observable, throwError } from 'rxjs';
import { ContentDetails } from '../Model/contentType';
import {UserDetails} from '../Model/UserDetails'
import { Feedback } from '../Model/feedback';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Idea } from '../Model/Idea';
import { Bug } from '../Model/bug';
import { BugStatus } from '../Model/bugStatus';
import { ReportUserList } from '../Model/reportUser';
import { LoginDetails } from '../Model/LoginDetails';
import { CreationDetails } from '../Model/CreationDetails';
import { UserHistory } from '../Model/UserHistory';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ContentFetchService {

  obj={
    type: 7,
    ContentIds:[
      {
        contentId: 0,
  delete: true
}
    ]

};
jsonobj=JSON.stringify(this.obj);



  constructor(private http: HttpClient) { }
  public URL: any = environment.baseURL+"/api/Content";

  getContentFetch(Type:any,PageNo:any , FromDate?:string, ToDate?:string,Filter?:boolean) :Observable<ContentDetails[]>{
    let mainURL =this.URL+"/"+PageNo+"?Type="+Type+"&Filter="+Filter+"&FromDate="+FromDate+"&ToDate="+ToDate
    return this.http.get<ContentDetails[]>(mainURL);
  }

  getVideo(Type:any,PageNo:any,IsReported?:any,Isverified?:any,IsDeleted?:any, FromDate?:string, ToDate?:string, coID?:any,pageszie?:any):Observable<ContentDetails[]>{
    let mainURL =
    this.URL+"/"+PageNo+"?Type="+Type+"&Isverified="+Isverified+"&IsDeleted="+IsDeleted+"&IsReported="+IsReported+"&FromDate="+FromDate+"&ToDate="+ToDate+"&CorelationId="+coID+"&Pagesize="+pageszie;
    return this.http.get<ContentDetails[]>(mainURL);
  }
  getPage(Type:any,PageNo:any, pageszie?:any) :Observable<ContentDetails[]>{
    let mainURL =this.URL+"/"+PageNo+"?Type="+Type+"&Pagesize="+pageszie
    return this.http.get<ContentDetails[]>(mainURL);
  }
  getContent(Type:any,PageNo:any) :Observable<ContentDetails[]>{
    return this.http.get<ContentDetails[]>(this.URL+"/"+PageNo+"?Type="+Type);
  }

  getUser(User:any) :Observable<UserDetails[]>{
  let userURL=  environment.baseURL+"/api/UserInfo/"+0+"?criteria="+2+"&searchstring="+User;
    return this.http.get<UserDetails[]>(userURL);
  }

  getPeople(PageNo?:any, PageSize?:any,serach?:any,criteria?:any) :Observable<UserDetails[]>{
    let userURL= environment.baseURL+ "/api/UserInfo/"+PageNo+"?criteria="+criteria+"&searchstring="+serach+'&Pagesize='+PageSize;
      return this.http.get<UserDetails[]>(userURL);
    }
  deleteContent(Type,id) :Observable<any>{
    let deleteURL = this.URL+"?Type="+Type+"&ContentIds="+id;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = deleteURL;
    return this.http.delete<any>(url, { headers })
      .pipe(
        tap(
          data => console.log('deleteProduct: ' + id)),
        catchError( this.handleError)

      );
    // return this.http.delete<string>(deleteURL);
  }
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  deletingContent(type,id, del,reasonForDelete){
    let deletedContentIds = [
      {
        contentId: id,
        delete: del,
        reasonId:reasonForDelete
      }
    ]
    let formData = new FormData();
      formData.append("Type",type)
      // for (var i =0; i<=id; i++){
        formData.append("ContentIds["+0+"][contentId]",id)
        formData.append("ContentIds["+0+"][delete]",del)
        formData.append("ContentIds["+0+"][reasonId]",reasonForDelete)
      // }
    return this.http.post(this.URL,formData);
  }


  blockUser(id, value){
    let mainURL =environment.baseURL+'/api/UserInfo/BlockUser';
    let formData = new FormData();
      formData.append("UserId",id)
      formData.append("IsBlockedUser",value)
    return this.http.post(mainURL,formData);
  }

loginDtails(userID) :Observable<LoginDetails>{
  let userURL= environment.baseURL+ "/api/UserInfo/UserLoginDetails/"+userID;
  return this.http.get<LoginDetails>(userURL);
}

creationDtails(userID) :Observable<CreationDetails>{
  let userURL= environment.baseURL+ "/api/UserInfo/UserContentDetails/"+userID;
  return this.http.get<CreationDetails>(userURL);
}

userHishtory(corelationID,PageNo?:any, PageSize?:any) :Observable<UserHistory[]>{
  let userURL= environment.baseURL+ "/api/UserInfo/UserHistoryDetails/"+corelationID+'?PageNo='+PageNo+'&Pagesize='+PageSize;
  return this.http.get<UserHistory[]>(userURL);
}

historyCount(userID) :Observable<any>{
  let userURL= environment.baseURL+ "/api/UserInfo/UserHistoryCount/"+userID;
  return this.http.get<any>(userURL);
}

  UpdateContent(Type,id) :Observable<void>{
    let UpdateURL = this.URL+"?Type="+Type+"&ContentIds="+id;
    return this.http.put<void>(UpdateURL,Type);
  }

  feedbackGet(pageNo,FromDate?:any,ToDateTime?:any,UserId?:any,PageSize?:any) :Observable<Feedback[]>{
    let UpdateURL = environment.baseURL+"/api/FeedBackandIdeas/"+pageNo+"?FromDate="+FromDate+"&ToDateTime="+ToDateTime+"&UserId="+UserId+"&PageSize="+PageSize;
    return this.http.get<Feedback[]>(UpdateURL);
  }

  ideaGet(pageNo,FromDate?:any,ToDateTime?:any,UserId?:any,PageSize?:any) :Observable<Idea[]>{
    let UpdateURL =environment.baseURL+ "/api/FeedBackandIdeas/GetAllIdeas/"+pageNo+"?FromDate="+FromDate+"&ToDateTime="+ToDateTime+"&UserId="+UserId+"&PageSize="+PageSize;
    return this.http.get<Idea[]>(UpdateURL);
  }
  bugGet(pageNo:any,FromDate?:any,ToDateTime?:any,bugStatus?:any,UserId?:any,PageSize?:any) :Observable<Bug[]>{
    let UpdateURL =environment.baseURL+ "/api/BugReport/"+pageNo+"?FromDate="+FromDate+"&ToDateTime="+ToDateTime+"&BugStatus="+bugStatus+"&UserId="+UserId+"&PageSize="+PageSize;
    return this.http.get<Bug[]>(UpdateURL);
  }
  getBugStatus() :Observable<BugStatus[]>{
    let UpdateURL =environment.baseURL+ "/api/BugReport/GetBugStatusList";
    return this.http.post<BugStatus[]>(UpdateURL,UpdateURL);
  }
  UpdateBugStatus(bugID,statusID?:any) :Observable<any>{
    let UpdateURL =environment.baseURL+ "/api/BugReport/UpdateBugStatus/"+bugID+"?BugStatus="+statusID;
    return this.http.post<any>(UpdateURL,UpdateURL);
  }  

  report(Type,ContentId):Observable<ReportUserList[]>{
    let reportURL= environment.baseURL+"/api/Content/ReportedByPeople?ContentId="+ContentId+"&Type="+Type
     return this.http.get<ReportUserList[]>(reportURL);

   }
}
