import { Component, OnInit, Input } from '@angular/core';
import { ContentTypeDetails } from '../Model/contentEnum';
import{AskService} from '../ask/ask.service'
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import { Observable } from 'rxjs';
import{ManageDeleteService} from '../manage-delete-table/service/manage-delete.service'
import { NgbModal, ModalDismissReasons,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ask-card',
  templateUrl: './ask-card.component.html',
  styleUrls: ['./ask-card.component.scss']
})
export class AskCardComponent implements OnInit {
  @Input() askDetails:any;
  allAsk:any;
  
  closeResult = '';
  deleteAskForm: FormGroup;
  isdisable:any;
  
  deleting:any;
  reasonForDelete:any;
  deleteId:any;
  nameValue:any;
  reportListUser=[];
  allreasons=[];
  contentId:any;
  textfeild=''
  constructor(private AskService:AskService,private ManageDeleteService: ManageDeleteService,private formBuilder: FormBuilder,private toastr: ToastrService,private modalService: NgbModal) { }

  ngOnInit(): void {
    if(this.askDetails.reasonId==null) {
      this.isdisable= true
    }
    else{
      this.isdisable= false
    }
    this.allAsk=this.askDetails
    this.deleteAskForm = this.formBuilder.group({
      // FromDate: '',
      // ToDate: '',
      // reported: [],
      // name:'',
      // corelationId:'',
      reasonId:0,
      // delete:false
    });
  }


  openThoughts(content,anyOtherViews) {
    this.textfeild=anyOtherViews;
    // this.titlefeild=title;

    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getTHoughtDismissReason(reason)}`;
    });
   
  }

  private getTHoughtDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  get dele() { return this.deleteAskForm.controls; }

  deleteConfirm(){
    // this.deleting=this.dele.delete.value;
    
    this.reasonForDelete=this.dele.reasonId.value;
    this.delete();
  }
  delete(){
    // console.log(this.deleteId);
    // this.userDetail.deleteAsk(ContentTypeDetails.Ask,this.deleteId).subscribe(data=>{
    //   // console.log('susscefully deleted'+data);
    //   this.modalService.dismissAll();
    // })
    this.AskService.deletingContent(ContentTypeDetails.Ask, this.deleteId,this.deleting,this.reasonForDelete).subscribe(data=>{
      // console.log(data);
      this.modalService.dismissAll()
      this.toastr.success(this.nameValue+' Successfully', 'Your Content', {
        timeOut: 3000
    });
    })
    // this.openModal=true;
  }
  verifying(){
    this.AskService.UpdateContent(ContentTypeDetails.Ask,this.deleteId).subscribe(data=>{
      // console.log('susscefully deleted'+data);
      // this.openModal=true;
      this.modalService.dismissAll();
      this.toastr.success('Verify Successfully', 'Your Content', {
        timeOut: 3000
    });
    })

  }


  reason(){
    this.ManageDeleteService.getDeleteReason().subscribe(data=>{
      this.allreasons=data
    })
  }
  open(content,id,boolValue,deleteValue) {
    this.reason()
    this.deleteId=id;
    this.deleting = boolValue;
    this.nameValue=deleteValue;
    // console.log(value);
    // this.deleteAskForm.get('reasonId').setValue(value);
    // this.deleteAskForm.get('delete').setValue(del);
    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  deleteopen(content,id,boolValue,deleteValue,value?:any) {
    this.reason()
    this.deleteId=id;
    this.deleting = boolValue;
    this.nameValue=deleteValue;
    // console.log(value);
    this.deleteAskForm.get('reasonId').setValue(value);
    // this.deleteAskForm.get('delete').setValue(del);
    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getdeleteDismissReason(reason)}`;
    });
   
  }

  private getdeleteDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  verify(contentt,id) {
    this.deleteId=id;
    // this.deleteThought(id);

    this.modalService.open(contentt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonal(reason)}`;
    });
   
  }
  private getDismissReasonal(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  reportContent(){
    this.AskService.report(ContentTypeDetails.Ask,this.contentId).subscribe(data=>{
      // console.log(data)
      this.reportListUser=data
    });
  }
  reportUser(reportUserList,id) {
    this.contentId=id;
    this.reportContent()
    // this.deleteId=id;
    // this.reason();
    // this.deleting = boolValue;
    // this.nameValue=name
    // console.log(boolValue);
    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(reportUserList, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonReported(reason)}`;
    });
   
  }
  private getDismissReasonReported(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
