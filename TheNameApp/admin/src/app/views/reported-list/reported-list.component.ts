import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-reported-list',
  templateUrl: './reported-list.component.html',
  styleUrls: ['./reported-list.component.scss']
})
export class ReportedListComponent implements OnInit {
  @Input() UserList:any;

  user:any;
  constructor() { }

  ngOnInit(): void {
    this.user=this.UserList;
  }

}
