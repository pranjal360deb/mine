import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { map, catchError, finalize, filter, take, switchMap,mapTo, tap } from "rxjs/operators";
import { Observable, throwError } from 'rxjs';
import { AllTags } from '../Model/AllTags';
import { ContentDetails } from '../../Model/ContentDetails';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AllTagsService {

  constructor(private http: HttpClient) { }
  public URL: any = environment.baseURL+"/api/HashTag";

  getAllTags(Search?:any,PageNo?:any,Pagesize?:any) :Observable<AllTags[]>{
    let mainURL =this.URL+'?Search='+Search+'&PageNo='+PageNo+'&Pagesize='+Pagesize;
    // let mainURL =this.URL
    return this.http.get<AllTags[]>(mainURL);
  }

  getContentTags(pageNo,type,tagname,pageSize?:any) :Observable<ContentDetails[]>{ 
    let mainURL =this.URL + "/TagContent/ "+pageNo+'?Tag='+tagname+'&type='+type+'&PageSize='+pageSize;
    return this.http.get<ContentDetails[]>(mainURL);
  }
}
