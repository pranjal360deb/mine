export interface AllTags {
    article: any,
    asks: any,
    images: any,
    music: any,
    notes: any,
    podCast: any,
    stories: any,
    tag: any,
    video: any,
    thought: any
}