import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {EnableAdsService} from './services/enable-ads.service'
@Component({
  selector: 'app-custom-ads',
  templateUrl: './custom-ads.component.html',
  styleUrls: ['./custom-ads.component.scss']
})
export class CustomAdsComponent implements OnInit {
  filterForm: FormGroup;
  closeResult=''
  adConfirm;
  adID
  Value
  constructor(private EnableAdsService:EnableAdsService, private toastr: ToastrService, private modalService: NgbModal,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.getadvalue();
    this.filterForm = this.formBuilder.group({
      
      ads: true
    });
  }
  getadvalue(){
    this.EnableAdsService.getContentFetch().subscribe(data=>{
      data.forEach(element => {
        // console.log();
        this.adID=element.adId
        this.filterForm.get('ads').setValue(element.adValue);
      });
    })
  }
  get f() { return this.filterForm.controls; }

  FieldsChange(event){
    //  this.filterForm.get('ads').setValue(event);
    this.adConfirm=event.currentTarget.checked;
        // console.log(this.adConfirm)
// console.log(event.currentTarget.checked);
this.updateAd()
  }
  updateAd(){
    this.EnableAdsService.UpdateAds(this.adID,this.adConfirm).subscribe(data=>{
      // console.log(data)
      this.toastr.success('Update Successfully', 'Ad Status', {
        timeOut: 3000
    });
    })
  }
  open(content) {
    // console.log(this.adConfirm)
    // console.log(this.bugID)
    // this.f.message=msg
    // this.filterForm.get('message').setValue(msg);
    // this.deleteThought(id);
    // console.log( this.reasonId);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }



}
