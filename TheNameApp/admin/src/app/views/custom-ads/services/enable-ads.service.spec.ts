import { TestBed } from '@angular/core/testing';

import { EnableAdsService } from './enable-ads.service';

describe('EnableAdsService', () => {
  let service: EnableAdsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnableAdsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
