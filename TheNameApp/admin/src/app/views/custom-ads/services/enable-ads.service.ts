import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { map, catchError, finalize, filter, take, switchMap,mapTo, tap } from "rxjs/operators";
import { Observable, throwError } from 'rxjs';
import { adDetails } from '../Model/adDetails';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EnableAdsService {

  constructor(private http: HttpClient) { }
  public URL: any =environment.baseURL+"/api/AdConfig";

  getContentFetch() :Observable<adDetails[]>{
    let mainURL =this.URL
    return this.http.get<adDetails[]>(mainURL);
  }
  UpdateAds(adID,Value?:string,) :Observable<any>{
    let mainURL =this.URL+"/UpdateAddconfig/"+adID+"?Value="+Value;
    return this.http.put<any>(mainURL,Value);
  }

}
