import { Component, OnInit } from '@angular/core';
import { ContentFetchService } from '../services/content-fetch.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import {ContentTypeDetails} from '../Model/contentEnum'
import {map, startWith} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DashService } from '../dashboard/dash.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;
  filterForm: FormGroup;
  fromdate:any;
  fromday:any;
  frommonth:any;
  fromyear:any;
  num:number;

  pagesall = 1;
collectionSize =100;
pagggee = 25
  coID:any;
  reported = [
      { id: 1, name: 'Reported' },
      { id: 2, name: 'Deleted' },
      { id: 3, name: 'Verified' },
  ];
  today:any;
  tomonth:any;
  toyear:any;
  repor:boolean = false;
  page: number = 0;
  todate:string;
  public userDetails =[]
  options: string[] = ['One', 'Two', 'Three'];
  public userList =[]

  videoReportedCount:number;
totalCount:number;
newCount:number;

  videoCount:number;
  
  feedbackCounting:number;
  ideaCounting:number;
  bugCounting:number;

  pageNo = [
   25,
   50,
   75,
   100
];
public contentDetails = [];
pages=[]
pagenum:any;
  pagsize:any;
  i:any;
  pagingFilter:number=0;

  IsReported: boolean = false;
  Isverified:boolean = false;
  IsDeleted:boolean = false;
  isLoading:boolean=false;
  isRefresh:boolean=false;
  constructor(private dashservice: DashService ,private formBuilder: FormBuilder,private datePipe: DatePipe,public ContentFetchService: ContentFetchService) { }

  ngOnInit(): void {
    this.getContentType();
    // this.default();
   this.getArticles();
   this.filterForm = this.formBuilder.group({
    FromDate: '',
    ToDate: '',
    reported: [],
    name:'',
    corelationId:'',
    IsReported: false,
    Isverified:false,
    IsDeleted:false,
  });
  this.filteredOptions = this.myControl.valueChanges.pipe(
    startWith(''),
    map(value => this._filter(value))
  );
  }

  loadPage(pageNumber: number) {
    // if (page !== this.previousPage) {
    //   this.previousPage = page;
    //   this.loadData();
    this.isLoading=false;
    this.userDetails=[];
    this.page = pageNumber - 1;
    // }
    this.getArticles()
    // console.log(this.page);
  }
  refresh(){
    this.isRefresh=true;
    this.userDetails=[];
    this.isLoading=false;
    this.getArticles();
  }
  getContentType(){
    this.dashservice.getDetails().subscribe(data=>{
      this.contentDetails=data;
      this.contentDetails.forEach(contentDetail => {

        switch(contentDetail.contentType){
          case ContentTypeDetails.Article: 
          {
            this.videoCount = contentDetail.totalCount;
            this.videoReportedCount = contentDetail.reportedCount;
            this.totalCount=contentDetail.totalCount
            this.newCount=this.totalCount-this.videoCount
            break;
          }

        }
        
      });
    })
  }
  default(){
    this.pages=[]
    this.getContentType()
    this.pagsize =25;
    this.pagenum = this.videoCount/this.pagsize;
    // console.log(this.pagenum);
    this.pageList();
  }
  next(paging){
    this.pagingFilter=paging
    this.pagination()
    // console.log("page number is"+page);
  }
  pageList(){
    for(this.i=0;this.i<=this.pagenum;this.i++){
      this.pages.push(this.i)
    }
  }
  pagination(){
    this.ContentFetchService.getPage(ContentTypeDetails.Article,this.pagingFilter,this.pagsize).subscribe(data=>{
      this.userDetails=data;
          // console.log(data);
    })
  }
  onOptionsSelected(event){
    this.pagggee=0
    this.pagggee = event.target.value;
    this.page=0

  //   this.pages=[]
  //   this.getContentType()
  //   this.pagsize = event.target.value;
  //   this.pagenum = this.videoCount/parseInt(this.pagsize);


  //   let num = parseInt(this.pagenum)
    
  //   this.pageList();
   this.getArticles()
    // console.log(value);
 
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  selectUser(id,name){

    this.filterForm.get('corelationId').setValue(id);
    this.filterForm.get('name').setValue(name);

  }
  onSearchChange(searchValue: string): void {  
    // console.log(searchValue);
    this.ContentFetchService.getUser(searchValue).subscribe(data=>{
      this.userList=data
      // console.log(data);
    })
  }

  get f() { return this.filterForm.controls; }

  filter(){
    this.isLoading=false;
    let rep = this.f.reported.value;
    this.repor = rep;
    this.num = rep;
    this.IsReported = this.f.IsReported.value;
    this.Isverified = this.f.Isverified.value;
    this.IsDeleted= this.f.IsDeleted.value;
    this.coID = this.f.corelationId.value;
    let fd = this.f.FromDate.value;
    // let date =this.datePipe.transform(fd,'yyyy-dd-MM');
    this.fromdate = this.datePipe.transform(fd,'yyyy-MM-dd');



    // this.today=this.f.ToDate.value.day;
    // this.tomonth=this.f.ToDate.value.month;
    // this.toyear=this.f.ToDate.value.year;

    // let tday = this.today.toString();
    // let tmonth = this.tomonth.toString();
    // let tyear = this.toyear.toString();
    // let tDate = tyear + "-" + tmonth + "-" + tday;
    let td = this.f.ToDate.value;
    this.todate=this.datePipe.transform(td,'yyyy-MM-dd');;

    this.userDetails=[];
    this.page=0;
    

    this.getArticles();

    // console.log(this.f.ToDate.value);

  }
  onScrolldown() {
    this.page = this.page + 1;
    this.getArticles();
  }
  getArticles(){
    // if(this.page==0)
    // { 
      this.ContentFetchService.getVideo(ContentTypeDetails.Article,this.page,this.IsReported,this.Isverified,this.IsDeleted,this.fromdate,this.todate,this.coID,this.pagggee).subscribe(data=>{
        this.isLoading=true;
        this.isRefresh=false;
        this.userDetails=data;
      })
  //  }
    //  else{
    //   this.ContentFetchService.getVideo(ContentTypeDetails.Article,this.page,this.fromdate,this.todate,this.repor,this.coID).subscribe(data=>{
    //     this.userDetails=this.userDetails.concat(data);;
    //   })
   
    //  }
  }

}
