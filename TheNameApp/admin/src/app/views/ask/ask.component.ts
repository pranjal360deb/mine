import { Component, OnInit } from '@angular/core';
import { AskService } from './ask.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import {ContentTypeDetails} from '../Model/contentEnum'
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { DashService } from '../dashboard/dash.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import{ManageDeleteService} from '../manage-delete-table/service/manage-delete.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-ask',
  templateUrl: './ask.component.html',
  styleUrls: ['./ask.component.scss']
})
export class AskComponent implements OnInit {
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]>;
  openModal:boolean = false;
  deleteAskForm: FormGroup;

  filterForm: FormGroup;
  fromdate:any;
  fromday:any;
  frommonth:any;
  fromyear:any;
  isdeleted:any;
  today:any;
  tomonth:any;
  toyear:any;

  repor:boolean = false;
  page: number = 0;
  todate:string;
  public userDetails =[]
  isLoading:boolean=false;
  deleteId:number;
  closeResult = '';

  num:number;
  coID:any;
  reported = [
      { id: 1, name: 'Reported' },
      { id: 2, name: 'Deleted' },
      { id: 3, name: 'Verified' },
  ];
  public userList =[]


  ImageCount:number;
  ImageReportedCount:number;
  ThoughtReportedCount:number;
  NoteReportedCount:number;
  AskReportedCount:number;
  ArticleReportedCount:number;
  StoryReportedCount:number;
  MusiceReportedCount:number;
  PodcastReportedCount:number;
  videoReportedCount:number;



  videoCount:number;
  PodcastCount:number;
  MusicCount:number;
  StoryCount:number;
  ArticleCount:number;
  AskCount:number;
  NoteCount:number;
  ThoughtCount:number;
  feedbackCounting:number;
  ideaCounting:number;
  bugCounting:number;

  pageNo = [
   25,
   50,
   75,
   100
];
public contentDetails = [];
pages=[]
pagenum:any;
  pagsize:any;
  i:any;
  pagingFilter:number=0;
  pagesall = 1;
  collectionSize =100;
  pagggee = 25

  IsReported: boolean = false;
  Isverified:boolean = false;
  IsDeleted:boolean = false;
  deleting;
reasonForDelete;
allreasons=[]
nameValue=''
contentId:any;
  reportListUser=[];
  isdisable: boolean=false;
  isRefresh:boolean=false;
  constructor(private toastr: ToastrService,private ManageDeleteService: ManageDeleteService,private modalService: NgbModal,private dashservice: DashService,private datePipe: DatePipe,private formBuilder: FormBuilder,public userDetail: AskService) { }

  ngOnInit(): void {
    this.getContentType();
    // this.default();
   this.getAsk();
    this.filterForm = this.formBuilder.group({
      FromDate: '',
      ToDate: [''],
      reported: [],
      name:'',
      corelationId:'',
      IsReported: false,
      Isverified:false,
      IsDeleted:false,    
    });
    this.deleteAskForm = this.formBuilder.group({
      // FromDate: '',
      // ToDate: '',
      // reported: [],
      // name:'',
      // corelationId:'',
      reasonId:0,
      // delete:false
    });
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }


  get dele() { return this.deleteAskForm.controls; }

  reason(){
    this.ManageDeleteService.getDeleteReason().subscribe(data=>{
      this.allreasons=data
    })
  }
  deleteConfirm(){
    // this.deleting=this.dele.delete.value;
    
    this.reasonForDelete=this.dele.reasonId.value;
    this.delete();
  }

  refresh(){
    this.isRefresh=true;
    this.userDetails=[];
    this.isLoading=false;
    this.getAsk();
  }
  loadPage(pageNumber: number) {
    // if (page !== this.previousPage) {
    //   this.previousPage = page;
    //   this.loadData();
    this.isLoading=false;
    this.userDetails=[];
    this.page = pageNumber - 1;
    // }
    this.getAsk()
    // console.log(this.page);
  }
  getContentType(){
    this.dashservice.getDetails().subscribe(data=>{
      this.contentDetails=data;
      this.contentDetails.forEach(contentDetail => {

        switch(contentDetail.contentType){
          case ContentTypeDetails.Ask: 
          {
            this.videoCount = contentDetail.totalCount;
            this.videoReportedCount = contentDetail.reportedCount;
            break;
          }

        }
        
      });
    })
  }
  default(){
    this.pages=[]
    this.getContentType()
    this.pagsize =25;
    this.pagenum = this.videoCount/this.pagsize;
    // console.log(this.pagenum);
    this.pageList();
  }
  next(paging){
    this.pagingFilter=paging
    this.pagination()
    // console.log("page number is"+page);
  }
  pageList(){
    for(this.i=0;this.i<=this.pagenum;this.i++){
      this.pages.push(this.i)
    }
  }
  pagination(){
    this.userDetail.getPage(ContentTypeDetails.Ask,this.pagingFilter,this.pagsize).subscribe(data=>{
      this.userDetails=data;
          // console.log(data);
    })
  }
  onOptionsSelected(event){
    this.pagggee=0
    this.pagggee = event.target.value;
    this.page=0

  //   this.pages=[]
  //   this.getContentType()
  //   this.pagsize = event.target.value;
  //   this.pagenum = this.videoCount/parseInt(this.pagsize);


  //   let num = parseInt(this.pagenum)
    
  //   this.pageList();
   this.getAsk()
    // console.log(value);
 
  }


  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  selectUser(id,name){

    this.filterForm.get('corelationId').setValue(id);
    this.filterForm.get('name').setValue(name);

  }
  onSearchChange(searchValue: string): void {  
    // console.log(searchValue);
    this.userDetail.getUserDetails(searchValue).subscribe(data=>{
      this.userList=data
      // console.log(data);
    })
  }

  getAsk(){
//     if(this.page==0)
//  { 
   this.userDetail.getUser(ContentTypeDetails.Ask,this.page,this.IsReported,this.Isverified,this.IsDeleted,this.fromdate,this.todate,this.coID,this.pagggee).subscribe(data=>{
    this.isLoading=true;
    this.isRefresh=false;
    // data.forEach(element => {
    //   console.log(element.reasonId);
    //   if(element.reasonId=null){
    //     this.isdisable=true;

    //   }
    //   else{
    //     this.isdisable=false;

    //   }
    // });
    this.userDetails=data;


    
  })
// }
//   else{
//     this.userDetail.getUser(ContentTypeDetails.Ask,this.page,this.fromdate,this.todate,this.repor,this.coID).subscribe(data=>{
//       this.userDetails= this.userDetails.concat(data);
//     })

//   }
  }
  deleteAsk(id){
    // console.log(id);
    this.userDetail.deleteAsk(ContentTypeDetails.Ask,id).subscribe(data=>{
      // console.log('susscefully deleted'+data);
    })
  }


  delete(){
    // console.log(this.deleteId);
    // this.userDetail.deleteAsk(ContentTypeDetails.Ask,this.deleteId).subscribe(data=>{
    //   // console.log('susscefully deleted'+data);
    //   this.modalService.dismissAll();
    // })
    this.userDetail.deletingContent(ContentTypeDetails.Ask, this.deleteId,this.deleting,this.reasonForDelete).subscribe(data=>{
      // console.log(data);
      this.modalService.dismissAll()
      this.toastr.success(this.nameValue+' Successfully', 'Your Content', {
        timeOut: 3000
    });
    })
    // this.openModal=true;
  }

  verifying(){
    this.userDetail.UpdateContent(ContentTypeDetails.Ask,this.deleteId).subscribe(data=>{
      // console.log('susscefully deleted'+data);
      // this.openModal=true;
      this.modalService.dismissAll();
      this.toastr.success('Verify Successfully', 'Your Content', {
        timeOut: 3000
    });
    })

  }
  onScrolldown() {
    this.page = this.page + 1;
    this.getAsk();
  }
  get f() { return this.filterForm.controls; }


  filter(){
    // this.fromday=this.f.FromDate.value.day;
    // this.frommonth=this.f.FromDate.value.month;
    // this.fromyear=this.f.FromDate.value.year;

    // let day = this.fromday.toString();
    // let month = this.frommonth.toString();
    // let year = this.fromyear.toString();
    // let fDate = year + "-" + month + "-" + day;

    // this.fromdate=fDate;
    this.isLoading=false;
    let rep = this.f.reported.value;
    this.repor = rep;
    let fd = this.f.FromDate.value;
    this.num = rep;
    this.coID = this.f.corelationId.value;
    // let date =this.datePipe.transform(fd,'yyyy-dd-MM');
    this.fromdate = this.datePipe.transform(fd,'yyyy-MM-dd');

    this.IsReported = this.f.IsReported.value;
    this.Isverified = this.f.Isverified.value;
    this.IsDeleted= this.f.IsDeleted.value;

    // this.today=this.f.ToDate.value.day;
    // this.tomonth=this.f.ToDate.value.month;
    // this.toyear=this.f.ToDate.value.year;

    // let tday = this.today.toString();
    // let tmonth = this.tomonth.toString();
    // let tyear = this.toyear.toString();
    // let tDate = tyear + "-" + tmonth + "-" + tday;
    let td = this.f.ToDate.value;
    this.todate=this.datePipe.transform(td,'yyyy-MM-dd');;

    this.userDetails=[];
    this.page=0;
    
this.getAsk();
  // this.userDetail.getUser(6,0,this.fromdate,this.todate,this.repor).subscribe(data=>{
  //     this.userDetails=data;
  //   })
    // console.log(this.f.ToDate.value);

  }


  open(content,id,boolValue,deleteValue) {
    this.reason()
    this.deleteId=id;
    this.deleting = boolValue;
    this.nameValue=deleteValue;
    // console.log(value);
    // this.deleteAskForm.get('reasonId').setValue(value);
    // this.deleteAskForm.get('delete').setValue(del);
    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  deleteopen(content,id,boolValue,deleteValue,value?:any) {
    this.reason()
    this.deleteId=id;
    this.deleting = boolValue;
    this.nameValue=deleteValue;
    console.log(value);
    this.deleteAskForm.get('reasonId').setValue(value);
    // this.deleteAskForm.get('delete').setValue(del);
    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getdeleteDismissReason(reason)}`;
    });
   
  }

  private getdeleteDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  verify(contentt,id) {
    this.deleteId=id;
    // this.deleteThought(id);

    this.modalService.open(contentt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonal(reason)}`;
    });
   
  }
  private getDismissReasonal(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  reportContent(){
    this.userDetail.report(ContentTypeDetails.Ask,this.contentId).subscribe(data=>{
      // console.log(data)
      this.reportListUser=data
    });
  }
  reportUser(reportUserList,id) {
    this.contentId=id;
    this.reportContent()
    // this.deleteId=id;
    // this.reason();
    // this.deleting = boolValue;
    // this.nameValue=name
    // console.log(boolValue);
    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(reportUserList, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonReported(reason)}`;
    });
   
  }
  private getDismissReasonReported(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
