import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AskDetails } from './askDetails';
import { UserDetails } from '../Model/UserDetails';
import { ContentDetails } from '../Model/contentType';
import { ReportUserList } from '../Model/reportUser';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AskService {

  constructor(private http: HttpClient) { }
  public URL: any = environment.baseURL+"/api/Content";

  getUser(Type:any,PageNo:any,IsReported?:any,Isverified?:any,IsDeleted?:any, FromDate?:string, ToDate?:string, coID?:any,pageszie?:any):Observable<AskDetails[]>{
    let mainURL =
    this.URL+"/"+PageNo+"?Type="+Type+"&Isverified="+Isverified+"&IsDeleted="+IsDeleted+"&IsReported="+IsReported+"&FromDate="+FromDate+"&ToDate="+ToDate+"&CorelationId="+coID+"&Pagesize="+pageszie;
    return this.http.get<AskDetails[]>(mainURL);
  }
  getAsk(Type:any,PageNo:any) :Observable<AskDetails[]>{
    return this.http.get<AskDetails[]>(this.URL+"/"+PageNo+"?Type="+Type);
  }
  deleteAsk(Type,id) :Observable<void>{
    let deleteURL = this.URL+"?Type="+Type+"&ContentIds="+id;
    return this.http.delete<void>(deleteURL);
  }
  getPage(Type:any,PageNo:any, pageszie?:any) :Observable<ContentDetails[]>{
    let mainURL =this.URL+"/"+PageNo+"?Type="+Type+"&Pagesize="+pageszie
    return this.http.get<ContentDetails[]>(mainURL);
  }
  getUserDetails(User:any) :Observable<UserDetails[]>{
    let userURL= environment.baseURL+ "/api/UserInfo/"+0+"?criteria="+2+"&searchstring="+User;
      return this.http.get<UserDetails[]>(userURL);
    }

    UpdateContent(Type,id) :Observable<void>{
      let UpdateURL = this.URL+"?Type="+Type+"&ContentIds="+id;
      return this.http.put<void>(UpdateURL,Type);
    }
    deletingContent(type,id, del,reasonForDelete){
      let deletedContentIds = [
        {
          contentId: id,
          delete: del,
          reasonId:reasonForDelete
        }
      ]
      let formData = new FormData();
        formData.append("Type",type)
        // for (var i =0; i<=id; i++){
          formData.append("ContentIds["+0+"][contentId]",id)
          formData.append("ContentIds["+0+"][delete]",del)
          formData.append("ContentIds["+0+"][reasonId]",reasonForDelete)
        // }
  
        // let httpParams = new HttpParams().set('aaa', '111');
        // httpParams.set('bbb', '222');
  
        // let opt = { params: httpParams };
  
  // this.http.delete('http://127.0.0.1:8000/api/employer/post_jobs/',options);
    //  const options = {
    //     headers: new HttpHeaders({
    //       'Content-Type': 'application/json'
    //     }),
    //   }
         
  
  
  
      // let userURL=  this.URL+"?Type="+type+"&ContentIds="+JSON.stringify(deletedContentIds);
  
  
      // let userURL=  this.URL+"?Type="+type+"&ContentIds="+id+"&delete="+del;
      return this.http.post(this.URL,formData);
    }
    report(Type,ContentId):Observable<ReportUserList[]>{
      let reportURL=environment.baseURL+"/api/Content/ReportedByPeople?ContentId="+ContentId+"&Type="+Type
       return this.http.get<ReportUserList[]>(reportURL);
  
     }
}
