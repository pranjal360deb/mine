import { Component, OnInit } from '@angular/core';
import { DashService } from './dash.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  public dashCounting:number;
  public contentDetails = [];
  ImageCount:number;
  ImageReportedCount:number;
  ThoughtReportedCount:number;
  NoteReportedCount:number;
  AskReportedCount:number;
  ArticleReportedCount:number;
  StoryReportedCount:number;
  MusiceReportedCount:number;
  PodcastReportedCount:number;
  videoReportedCount:number;



  videoCount:number;
  PodcastCount:number;
  MusicCount:number;
  StoryCount:number;
  ArticleCount:number;
  AskCount:number;
  NoteCount:number;
  ThoughtCount:number;
  feedbackCounting:number;
  ideaCounting:number;
  bugCounting:number;
  isRefresh:boolean=false;
  constructor(public DashService: DashService) { }

  ngOnInit(): void {
    // this.DashService.getUser().subscribe(data=>{
    //   this.dashCounting=data;
    // });
    this.getUserList();
    this.getContentType();
    this.getfeedbackcount();
    this.getIdeacount();
    this.getBugcount();
  }
  refresh(){
    this.isRefresh=true;
    this.getUserList();
    this.getContentType();
    this.getfeedbackcount();
    this.getIdeacount();
    this.getBugcount();  
    this.isRefresh=false;

  }
  getUserList(){
    this.DashService.getUser().subscribe(data=>{
      this.dashCounting=data;
    });
  }
  getfeedbackcount(){
    this.DashService.getFeedbackCount().subscribe(data=>{
      this.feedbackCounting=data;
    });
  }
  getIdeacount(){
    this.DashService.getIdeaCount().subscribe(data=>{
      this.ideaCounting=data;
    });
  }

  getBugcount(){
    this.DashService.getBugCount().subscribe(data=>{
      this.bugCounting=data;
    });
  }
  getContentType(){
    this.DashService.getDetails().subscribe(data=>{
      this.contentDetails=data;
      this.contentDetails.forEach(contentDetail => {

        switch(contentDetail.contentType){
          case 0: 
          {
            this.ImageCount = contentDetail.totalCount;
            this.ImageReportedCount = contentDetail.reportedCount;
            break;
          }
          case 1: 
          {
            this.videoCount = contentDetail.totalCount;
            this.videoReportedCount = contentDetail.reportedCount;

            break;
          }

          case 2: 
          {
            this.PodcastCount = contentDetail.totalCount;
            this.PodcastReportedCount = contentDetail.reportedCount;

            break;
          }
          case 3: 
          {
            this.MusicCount = contentDetail.totalCount;
            this.MusiceReportedCount = contentDetail.reportedCount;

            break;
          }
          case 4: 
          {
            this.StoryCount = contentDetail.totalCount;
            this.StoryReportedCount = contentDetail.reportedCount;

            break;
          }
          case 5: 
          {
            this.ArticleCount = contentDetail.totalCount;
            this.ArticleReportedCount = contentDetail.reportedCount;

            break;
          }
          case 6: 
          {
            this.AskCount = contentDetail.totalCount;
            this.AskReportedCount = contentDetail.reportedCount;

            break;
          }
          // case 7: 
          // {
          //   this.PollCount = contentDetail.totalCount;
          //   break;
          // }
          case 8: 
          {
            this.NoteCount = contentDetail.totalCount;
            this.NoteReportedCount = contentDetail.reportedCount;

            break;
          }
          case 9: 
          {
            this.ThoughtCount = contentDetail.totalCount;
            this.ThoughtReportedCount = contentDetail.reportedCount;
            break;
          }




        }
        
      });
    })
  }
}
