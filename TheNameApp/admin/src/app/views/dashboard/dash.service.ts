import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DashCount } from './dashCount';
import { Observable } from 'rxjs';
import {ContentDetails} from '../Model/contentType';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class DashService {
  ContentDetails = [];
  constructor(private http: HttpClient) { }
  public askURL: any =environment.baseURL+"/api/UserInfo/UserCount";  
  public URL: any = environment.baseURL+"/api/Content/ContentCount";

  getUser() :Observable<number>{
    return this.http.get<number>(this.askURL);
  }
  getFeedbackCount() :Observable<number>{
    let feedbackcount = environment.baseURL+"/api/FeedBackandIdeas/FeedBackCount";
    return this.http.get<number>(feedbackcount);
  }
  getIdeaCount() :Observable<number>{
    let ideacount =environment.baseURL+"/api/FeedBackandIdeas/IdeasCount";
    return this.http.get<number>(ideacount);
  }
  getBugCount() :Observable<number>{
    let ideacount = environment.baseURL+"/api/BugReport/BugCount";
    return this.http.get<number>(ideacount);
  }

  getDetails() :Observable<ContentDetails[]>{
    return this.http.get<ContentDetails[]>(this.URL);
  }

}
