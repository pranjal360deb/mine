import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ContentTypeDetails } from '../Model/contentEnum';
import { DatePipe } from '@angular/common';
import {ContentFetchService} from '../services/content-fetch.service'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import{ManageDeleteService} from '../manage-delete-table/service/manage-delete.service'
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-video-card',
  templateUrl: './video-card.component.html',
  styleUrls: ['./video-card.component.scss']
})
export class VideoCardComponent implements OnInit {
  filterForm: FormGroup;
  @Input() videodetails:any;
  imageString:any ;
  images:any
blobIDs:any;
hide: boolean = false;
type:any;
date:any;
  video:any;
  closeResult = '';
  deleteId:number;
  openModal:boolean = false;
  isdeleted:boolean = false;

  deleting;
  reasonForDelete;
  allreasons=[]

  nameValue=''
  contentId:any;
  reportListUser=[];
  isdisable:boolean = false;
  constructor(private toastr: ToastrService, private ManageDeleteService: ManageDeleteService,private formBuilder: FormBuilder,private datePipe: DatePipe,private modalService: NgbModal, private ContentFetchService:ContentFetchService ) { }

  ngOnInit(): void {
    if(this.videodetails.reasonId==null) {
      this.isdisable= true
    }
    else{
      this.isdisable= false
    }
    this.video=this.videodetails;
    this.blob();
    this.filterForm = this.formBuilder.group({
      // FromDate: '',
      // ToDate: '',
      // reported: [],
      // name:'',
      // corelationId:'',
      reasonId:0,
      // delete:false
    });

  }

  reason(){
    this.ManageDeleteService.getDeleteReason().subscribe(data=>{
      this.allreasons=data
    })
  }

  filter(){
    // this.deleting=this.f.delete.value;
    
    this.reasonForDelete=this.f.reasonId.value;
    this.delete();
  }
  get f() { return this.filterForm.controls; }
  open(){
    if(this.hide==false){
      this.hide=true;
    }
    else{
      this.hide=false;
    }
  }
  delete(){
    // this.ContentFetchService.deleteContent(ContentTypeDetails.Video,this.deleteId).subscribe(data=>{
    //   // console.log('susscefully deleted'+data);
    //   // this.openModal=true;
    //   this.modalService.dismissAll()

    // })
    // console.log(this.deleteId);
    // this.isdeleted=true;
    this.ContentFetchService.deletingContent(ContentTypeDetails.Video, this.deleteId,this.deleting,this.reasonForDelete).subscribe(data=>{
      // console.log(data);
      this.modalService.dismissAll()
      this.toastr.success(this.nameValue+' Successfully', 'Your Content', {
        timeOut: 3000
    });
    })
  }

  verifying(){
    this.ContentFetchService.UpdateContent(ContentTypeDetails.Video,this.deleteId).subscribe(data=>{
      // console.log('susscefully deleted'+data);
      // this.activeModal.close();
      this.modalService.dismissAll()
      this.toastr.success('Verify Successfully', 'Your Content', {
        timeOut: 3000
    });
    })

  }
  blob(){
    
    this.videodetails.medias.forEach(element => {
      this.blobIDs= element;
      this.type=this.blobIDs.contentType;
      this.imageString= 
     environment.baseURL +"/BlobFile/"+this.blobIDs.blobId+"?ContentType="+this.blobIDs.contentType;
    });

    this.images=environment.baseURL+"/BlobFile/"+this.videodetails.thumbnail.blobId+"?ContentType="+this.videodetails.thumbnail.contentType;

    // console.log(this.videodetails.thumbnail.blobId)

  }

  opening(content,id,boolValue,name,value?:any) {
    this.reason();
    this.deleteId=id;
    this.deleting = boolValue;
    this.nameValue=name
    this.filterForm.get('reasonId').setValue(value);

    // this.filterForm.get('delete').setValue(del);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  verify(contentt,id) {
    this.deleteId=id;
    // this.deleteThought(id);

    this.modalService.open(contentt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonal(reason)}`;
    });
   
  }
  private getDismissReasonal(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  reportContent(){
    this.ContentFetchService.report(ContentTypeDetails.Video,this.contentId).subscribe(data=>{
      // console.log(data)
      this.reportListUser=data
    });
  }
  reportUser(reportUserList,id) {
    this.contentId=id;
    this.reportContent();
    // this.deleteId=id;
    // this.reason();
    // this.deleting = boolValue;
    // this.nameValue=name
    // console.log(boolValue);
    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(reportUserList, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonReported(reason)}`;
    });
   
  }
  private getDismissReasonReported(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
