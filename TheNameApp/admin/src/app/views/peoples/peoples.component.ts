import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-peoples',
  templateUrl: './peoples.component.html',
  styleUrls: ['./peoples.component.scss']
})
export class PeoplesComponent implements OnInit {
  filterForm: FormGroup;
  myControl = new FormControl();
  Message:any
  description:any;
  closeResult = '';
  AdminId=1;
  AlertType=0
  constructor(private toastr: ToastrService, private modalService: NgbModal,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.filterForm = this.formBuilder.group({
      message: '',
      // ToDate: [''],
      // reported: [],
      // Description:'',
      // corelationId:'',    
    });
  }
  get f() { return this.filterForm.controls; }
  filter(){
    this.Message = this.f.message.value;
    // this.repor = rep;
    // this.description = this.f.Description.value; 
    // console.log("this is reason"+this.repor);
    // console.log("this is reason"+fd);
    // this.post()
  }
  open(content) {
    // this.deleteId=id;
    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
