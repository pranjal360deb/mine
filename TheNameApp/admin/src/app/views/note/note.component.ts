import { Component, OnInit } from '@angular/core';
import { ContentFetchService } from '../services/content-fetch.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import {ContentTypeDetails} from '../Model/contentEnum'
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { DashService } from '../dashboard/dash.service';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]>;

  filterForm: FormGroup;
  fromdate:any;
  fromday:any;
  frommonth:any;
  fromyear:any;
  num:number;
  coID:any;
  reported = [
   { id: 1, name: 'Reported' },
   { id: 2, name: 'Deleted' },
   { id: 3, name: 'Verified' },
];


  today:any;
  tomonth:any;
  toyear:any;
  repor:boolean = false;
  page: number = 0;
  todate:string;
  public userDetails =[]
  public userList =[]


  ImageCount:number;
  ImageReportedCount:number;
  ThoughtReportedCount:number;
  NoteReportedCount:number;
  AskReportedCount:number;
  ArticleReportedCount:number;
  StoryReportedCount:number;
  MusiceReportedCount:number;
  PodcastReportedCount:number;
  videoReportedCount:number;



  videoCount:number;
  PodcastCount:number;
  MusicCount:number;
  StoryCount:number;
  ArticleCount:number;
  AskCount:number;
  NoteCount:number;
  ThoughtCount:number;
  feedbackCounting:number;
  ideaCounting:number;
  bugCounting:number;

  pageNo = [
   25,
   50,
   75,
   100
];
public contentDetails = [];
pages=[]
pagenum:any;
  pagsize:any;
  i:any;
  pagingFilter:number=0;
  pagesall = 1;
  collectionSize:any;
  pagggee = 25
  IsReported: boolean = false;
  Isverified:boolean = false;
  IsDeleted:boolean = false;
  isLoading:boolean=false;
  isRefresh:boolean=false;
  constructor(private dashservice: DashService , private formBuilder: FormBuilder,private datePipe: DatePipe,public ContentFetchService: ContentFetchService) { }

  ngOnInit(): void {
    this.getContentType();
    // this.default();
   this.getNotes();
   this.filterForm = this.formBuilder.group({
    FromDate: '',
    ToDate: '',
    reported: [],
    IsReported: false,
    Isverified:false,
    IsDeleted:false,
    name:'',
    corelationId:''
  });
  this.filteredOptions = this.myControl.valueChanges.pipe(
    startWith(''),
    map(value => this._filter(value))
  );
  }
  refresh(){
    this.isRefresh=true;
    this.userDetails=[];
    this.isLoading=false;
    this.getNotes();
  }
  loadPage(pageNumber: number) {
    // if (page !== this.previousPage) {
    //   this.previousPage = page;
    //   this.loadData();
    this.isLoading=false;
    this.userDetails=[];
    this.page = pageNumber - 1;
    // }
    this.getNotes()
    // console.log(this.page);
  }
  getContentType(){
    this.dashservice.getDetails().subscribe(data=>{
      this.contentDetails=data;
      this.contentDetails.forEach(contentDetail => {

        switch(contentDetail.contentType){
          case ContentTypeDetails.Note: 
          {
            this.videoCount = contentDetail.totalCount;
            this.videoReportedCount = contentDetail.reportedCount;
            break;
          }

        }
        
      });
    })
  }
  default(){
    this.pages=[]
    this.getContentType()
    this.pagsize =25;
    this.pagenum = this.videoCount/this.pagsize;
    // console.log(this.pagenum);
    this.pageList();
  }
  next(paging){
    this.pagingFilter=paging
    this.pagination()
    // console.log("page number is"+page);
  }
  pageList(){
    for(this.i=0;this.i<=this.pagenum;this.i++){
      this.pages.push(this.i)
    }
  }
  pagination(){
    this.ContentFetchService.getPage(ContentTypeDetails.Note,this.pagingFilter,this.pagsize).subscribe(data=>{
      this.userDetails=data;
          // console.log(data);
    })
  }
  onOptionsSelected(event){
    this.pagggee=0
    this.pagggee = event.target.value;
    this.page=0
  //   this.pages=[]
  //   this.getContentType()
  //   this.pagsize = event.target.value;
  //   this.pagenum = this.videoCount/parseInt(this.pagsize);


  //   let num = parseInt(this.pagenum)
    
  //   this.pageList();
   this.getNotes()
    // console.log(value);
 
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  selectUser(id,name){

    this.filterForm.get('corelationId').setValue(id);
    this.filterForm.get('name').setValue(name);

  }
  onSearchChange(searchValue: string): void {  
    // console.log(searchValue);
    this.ContentFetchService.getUser(searchValue).subscribe(data=>{
      this.userList=data
      // console.log(data);
    })
  }

  getNotes(){
    // if(this.page==0)
    // { 
      this.ContentFetchService.getVideo(ContentTypeDetails.Note,this.page,this.IsReported,this.Isverified,this.IsDeleted,this.fromdate,this.todate,this.coID,this.pagggee).subscribe(data=>{
        this.isLoading=true;
        this.isRefresh=false;
        this.userDetails=data;
        // this.collectionSize=this.userDetails.length
        // console.log(this.userDetails.length);
      })
  //  }
  //    else{
  //     this.ContentFetchService.getVideo(ContentTypeDetails.Note,this.page,this.fromdate,this.todate,this.repor,this.coID).subscribe(data=>{
  //       this.userDetails=this.userDetails.concat(data);;
  //     })
   
  //    }
  }
  get f() { return this.filterForm.controls; }

  filter(){
    this.isLoading=false;
    let rep = this.f.reported.value;
    this.repor = rep;
    this.num = rep;
    this.coID = this.f.corelationId.value;
    let fd = this.f.FromDate.value;
    // let date =this.datePipe.transform(fd,'yyyy-dd-MM');
    this.fromdate = this.datePipe.transform(fd,'yyyy-MM-dd');

    this.IsReported = this.f.IsReported.value;
    this.Isverified = this.f.Isverified.value;
    this.IsDeleted= this.f.IsDeleted.value;

    // this.today=this.f.ToDate.value.day;
    // this.tomonth=this.f.ToDate.value.month;
    // this.toyear=this.f.ToDate.value.year;

    // let tday = this.today.toString();
    // let tmonth = this.tomonth.toString();
    // let tyear = this.toyear.toString();
    // let tDate = tyear + "-" + tmonth + "-" + tday;
    let td = this.f.ToDate.value;
    this.todate=this.datePipe.transform(td,'yyyy-MM-dd');;


    this.userDetails=[];
    this.page=0;

    this.getNotes();

    // console.log(this.f.ToDate.value);

  }

  onScrolldown() {
    this.page = this.page + 1;
    this.getNotes();
  }
}
