import { Component, OnInit, Input } from '@angular/core';
import {PhotoService} from '../photo/photo.service'
import {DomSanitizer} from '@angular/platform-browser'
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ContentTypeDetails} from '../Model/contentEnum'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import{ManageDeleteService} from '../manage-delete-table/service/manage-delete.service'
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-photo-card',
  templateUrl: './photo-card.component.html',
  styleUrls: ['./photo-card.component.scss']
})
export class PhotoCardComponent implements OnInit {
  @Input() photodetails:any;
  imageString:any ;
  blobIDs:any;
  contentType:any;
  photo:any;
  closeResult = '';
  deleteId:number;
  openModal:boolean = false;
  img =[];
  filterForm: FormGroup;
  deleting;
  reasonForDelete;
  allreasons=[]

  nameValue=''
  contentId:any;
  reportListUser=[];
  isdisable: boolean = false;
  imag=[]
  titlefeild=''
  constructor(private toastr: ToastrService,private ManageDeleteService: ManageDeleteService,private formBuilder: FormBuilder,private PhotoService:PhotoService,private sanitizer:DomSanitizer,private modalService: NgbModal) { }

  ngOnInit(): void {
    if(this.photodetails.reasonId==null) {
      this.isdisable= true
    }
    else{
      this.isdisable= false
    }
    this.photo=this.photodetails;
    this.blob();
    // this.getblobfile()
    this.filterForm = this.formBuilder.group({
      // FromDate: '',
      // ToDate: '',
      // reported: [],
      // name:'',
      // corelationId:'',
      reasonId:0,
      // delete:false
    });
  }
  
  blob(){
    this.photodetails.medias.forEach(element => {
      // console.log(element.blobId);
      this.blobIDs= element;
      this.imageString= environment.baseURL+"/BlobFile/"+this.blobIDs.blobId+"?ContentType="+this.blobIDs.contentType;
      // console.log(this.blobIDs.blobId);
      // console.log(this.blobIDs.contentType);
      // this.blobIDs=this.blobIDs.concat(element.contentType);
      // this.PhotoService.getBlobFile(this.blobIDs.blobId,this.blobIDs.contentType).subscribe(data=>{
      //   // console.log(data);
      //   // let objectURL = URL.createObjectURL(data);       
      //   // this.imageString = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        
      //   this.imageString=data;
      // })
      this.img.push(this.imageString);

    });

  }
  method2(content,img,title) {
    this.imag=img
    // console.log(img)
    // this.textfeild=anyOtherViews;
    this.titlefeild=title;

    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getPhotoDismissReason(reason)}`;
    });
   
  }

  private getPhotoDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  filter(){
    // this.deleting=this.f.delete.value;
    
    this.reasonForDelete=this.f.reasonId.value;
    this.delete();
  }
  get f() { return this.filterForm.controls; }

  delete(){
    // this.PhotoService.deletePhoto(ContentTypeDetails.Image,this.deleteId).subscribe(data=>{
    //   // console.log('susscefully deleted'+data);
    //   // this.openModal=true;
    //   this.modalService.dismissAll();

    // })

    this.PhotoService.deletingContent(ContentTypeDetails.Image, this.deleteId,this.deleting,this.reasonForDelete).subscribe(data=>{
      this.modalService.dismissAll()
      this.toastr.success(this.nameValue+' Successfully', 'Your Content', {
                    timeOut: 3000
                });
    })
    // this.openModal=true;
  }

  reason(){
    this.ManageDeleteService.getDeleteReason().subscribe(data=>{
      this.allreasons=data
    })
  }
  verifying(){
    this.PhotoService.UpdateContent(ContentTypeDetails.Image,this.deleteId).subscribe(data=>{
      // console.log('susscefully deleted'+data);
      // this.openModal=true;
      this.modalService.dismissAll();
      this.toastr.success('Verify Successfully', 'Your Content', {
                    timeOut: 3000
                });

    })

  }
  open(content,id,boolValue,name,value?:any) {
    this.deleteId=id;
    this.reason();
    this.deleting = boolValue;
    this.nameValue=name;
    this.filterForm.get('reasonId').setValue(value);
    // console.log(boolValue);
    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  reportContent(){
    this.PhotoService.report(ContentTypeDetails.Image,this.contentId).subscribe(data=>{
      // console.log(data)
      this.reportListUser=data
    });
  }
  reportUser(reportUserList,id) {
    this.contentId=id;
    this.reportContent();
    // console.log(id)
    // this.deleteId=id;
    // this.reason();
    // this.deleting = boolValue;
    // this.nameValue=name
    // console.log(boolValue);
    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(reportUserList, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonReported(reason)}`;
    });
   
  }
  private getDismissReasonReported(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  verify(contentt,id) {
    this.deleteId=id;
    // this.deleteThought(id);

    this.modalService.open(contentt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonal(reason)}`;
    });
   
  }
  private getDismissReasonal(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  // getblobfile(){

  //   this.PhotoService.getBlobFile(this.blobIDs.blobId,this.blobIDs.contentType).subscribe(data=>{

  //     this.imageString=data;
  //   })
  // }
}
