import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ContentFetchService } from '../services/content-fetch.service'
import { ContentTypeDetails } from '../Model/contentEnum';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ManageDeleteService } from '../manage-delete-table/service/manage-delete.service'
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-music-card',
  templateUrl: './music-card.component.html',
  styleUrls: ['./music-card.component.scss']
})
export class MusicCardComponent implements OnInit {

  @Input() musicDetails: any;
  music: any;
  imageString: any;
  images: any
  blobIDs: any;
  hide: boolean = false;
  type: any;
  closeResult = '';
  deleteId: number;
  openModal: boolean = false;
  deleting;
  reasonForDelete;
  allreasons = []
  filterForm: FormGroup;
  nameValue = ''
  path = 'assets/img/Default_Photo@1x.png';
  contentId:any;
  reportListUser=[];
  isdisable:boolean=false;
  constructor(private toastr: ToastrService, private ManageDeleteService: ManageDeleteService, private formBuilder: FormBuilder, private modalService: NgbModal, private ContentFetchService: ContentFetchService) { }

  ngOnInit(): void {
    if(this.musicDetails.reasonId==null) {
      this.isdisable= true
    }
    else{
      this.isdisable= false
    }
    this.music = this.musicDetails;
    this.blob();
    this.filterForm = this.formBuilder.group({
      // FromDate: '',
      // ToDate: '',
      // reported: [],
      // name:'',
      // corelationId:'',
      reasonId: 0,
      // delete:false
    });
  }
  open() {
    if (this.hide == false) {
      this.hide = true;
    }
    else {
      this.hide = false;
    }
  }
  reason() {
    this.ManageDeleteService.getDeleteReason().subscribe(data => {
      this.allreasons = data
    })
  }
  filter() {
    // this.deleting=this.f.delete.value;

    this.reasonForDelete = this.f.reasonId.value;
    this.delete();
  }
  get f() { return this.filterForm.controls; }
  blob() {

    this.musicDetails.medias.forEach(element => {
      this.blobIDs = element;
      this.type = this.blobIDs.contentType;
      this.imageString =
      environment.baseURL+ "/BlobFile/" + this.blobIDs.blobId + "?ContentType=" + this.blobIDs.contentType;
    });

    if (this.musicDetails.thumbnail != null) {
      this.images = environment.baseURL+"/BlobFile/" + this.musicDetails.thumbnail.blobId + "?ContentType=" + this.musicDetails.thumbnail.contentType;
    }
    else {
      this.images = this.path
    }
    // console.log(this.videodetails.thumbnail.blobId)

  }

  delete() {
    // this.ContentFetchService.deleteContent(ContentTypeDetails.Music,this.deleteId).subscribe(data=>{
    //   // console.log('susscefully deleted'+data);
    //   // this.openModal=true;
    //   this.modalService.dismissAll();

    // })
    this.ContentFetchService.deletingContent(ContentTypeDetails.Music, this.deleteId, this.deleting, this.reasonForDelete).subscribe(data => {
      // console.log(data);
      this.modalService.dismissAll()
      this.toastr.success(this.nameValue+' Successfully', 'Your Content', {
        timeOut: 3000
      });
    })
    // console.log(this.deleteId);
    // this.openModal=true;
  }

  verifying() {
    this.ContentFetchService.UpdateContent(ContentTypeDetails.Music, this.deleteId).subscribe(data => {
      // console.log('susscefully deleted'+data);
      // this.openModal=true;
      this.modalService.dismissAll();
      this.toastr.success('Verify Successfully', 'Your Content', {
        timeOut: 3000
      });
    })

  }
  deleteMusic(content, id, boolValue, name,value?:any) {
    this.reason()
    this.deleteId = id;
    this.deleting = boolValue;
    this.nameValue = name;
    this.filterForm.get('reasonId').setValue(value);

    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  verifyMusic(contentt, id) {
    this.deleteId = id;
    // this.deleteThought(id);

    this.modalService.open(contentt, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonal(reason)}`;
    });

  }
  private getDismissReasonal(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  reportContent(){
    this.ContentFetchService.report(ContentTypeDetails.Note,this.contentId).subscribe(data=>{
      // console.log(data)
      this.reportListUser=data
    });
  }
  reportUser(reportUserList,id) {
    this.contentId=id;
    this.reportContent();
    // this.deleteId=id;
    // this.reason();
    // this.deleting = boolValue;
    // this.nameValue=name
    // console.log(boolValue);
    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(reportUserList, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonReported(reason)}`;
    });
   
  }
  private getDismissReasonReported(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
