import { Component, OnInit } from '@angular/core';
import { ContentFetchService } from '../services/content-fetch.service';
import { DashService } from '../dashboard/dash.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  filterForm: FormGroup;
  public userDetails =[]
  page: number = 0;
  public contentDetails = [];
  feedbackCounting:number;

  pagesall = 1;
  collectionSize =100;
  pagggee = 25

  pageNo = [
    25,
    50,
    75,
    100
 ];
 userList=[];
 coID:any
  fromdate:any
  todate:any;
  isRefresh:boolean=false;
  isLoading:boolean=false;
  constructor(private datePipe: DatePipe,private formBuilder: FormBuilder,private dashservice: DashService ,public ContentFetchService: ContentFetchService) { }

  ngOnInit(): void {
    this.getFeedback();
    this.getfeedbackcount();
    this.filterForm = this.formBuilder.group({
      FromDate: '',
      ToDate: '',
      reported: [],
      name:'',
      corelationId:'',
      IsReported: false,
      Isverified:false,
      IsDeleted:false,
    });

  }
  refresh(){
    this.isRefresh=true;
    this.userDetails=[];
    this.isLoading=false;
    this.getFeedback();
  }
  selectUser(id,name){

    this.filterForm.get('corelationId').setValue(id);
    this.filterForm.get('name').setValue(name);

  }
  onSearchChange(searchValue: string): void {  
    // console.log(searchValue);
    this.ContentFetchService.getUser(searchValue).subscribe(data=>{
      this.userList=data
      // console.log(data);
    })
  }
  get f() { return this.filterForm.controls; }
  filter(){
    this.isLoading=false;
    this.userDetails=[];
    this.coID = this.f.corelationId.value;
    let fd = this.f.FromDate.value;
    // let date =this.datePipe.transform(fd,'yyyy-dd-MM');
    this.fromdate = this.datePipe.transform(fd,'yyyy-MM-dd');
    let td = this.f.ToDate.value;
    this.todate=this.datePipe.transform(td,'yyyy-MM-dd');;
    this.page=0;
    this.getFeedback();

  }

  loadPage(pageNumber: number) {
    // if (page !== this.previousPage) {
    //   this.previousPage = page;
    //   this.loadData();
    this.isLoading=false;
    this.userDetails=[];
    this.page = pageNumber - 1;
    // }
    this.getFeedback()
    // console.log(this.page);
  }

  getFeedback(){
    // if(this.page==0)
    // { 
      this.ContentFetchService.feedbackGet(this.page,this.fromdate,this.todate,this.coID,this.pagggee).subscribe(data=>{
        this.isLoading=true;
        this.isRefresh=false;
        this.userDetails=data;
      })
  //  }
  //    else{
  //     this.ContentFetchService.feedbackGet(this.page).subscribe(data=>{
  //       this.userDetails=this.userDetails.concat(data);;
  //     })
   
  //    }
  }
  getfeedbackcount(){
    this.dashservice.getFeedbackCount().subscribe(data=>{
      this.feedbackCounting=data;
    });
  }

  onOptionsSelected(event){
    this.pagggee=0
    this.pagggee = event.target.value;
    this.page=0
  //   this.pages=[]
  //   this.getContentType()
  //   this.pagsize = event.target.value;
  //   this.pagenum = this.videoCount/parseInt(this.pagsize);


  //   let num = parseInt(this.pagenum)
    
  //   this.pageList();
   this.getFeedback();
    // console.log(value);
 
  }


}
