import { Component, OnInit } from '@angular/core';
import { AskService } from '../ask/ask.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import {ContentTypeDetails} from '../Model/contentEnum'
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { DashService } from '../dashboard/dash.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import{ManageDeleteService} from '../manage-delete-table/service/manage-delete.service'
@Component({
  selector: 'app-manage-delete',
  templateUrl: './manage-delete.component.html',
  styleUrls: ['./manage-delete.component.scss']
})
export class ManageDeleteComponent implements OnInit {
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]>;
  openModal:boolean = false;

  filterForm: FormGroup;
 
  closeResult = '';

  public deleteDetails =[]
 
  
  public userList =[]


 

   pageNo = [
   25,
   50,
   75,
   100
];

  reason:any
  description:any;

  constructor(private toastr: ToastrService,private ManageDeleteService: ManageDeleteService, private modalService: NgbModal,private dashservice: DashService,private datePipe: DatePipe,private formBuilder: FormBuilder,public userDetail: AskService) { }

  ngOnInit(): void {
  //  this.getDeletes();
    this.filterForm = this.formBuilder.group({
      Reason: '',
      // ToDate: [''],
      // reported: [],
      Description:'',
      // corelationId:'',    
    });
  }


  
  postReson(){
    this.ManageDeleteService.postDeleteReason(this.reason,this.description).subscribe(data=>{
    // console.log(data);
    this.modalService.dismissAll();
    this.toastr.success('Submit Successfully', 'Your Reason', {
      timeOut: 3000
  });
      this.filterForm.get('Reason').setValue('');
    this.filterForm.get('Description').setValue('');


})
  }


  getDeletes(){
    this.ManageDeleteService.getDeleteReason().subscribe(data=>{
      this.deleteDetails=data;
    })

}

  get f() { return this.filterForm.controls; }


  filter(){
    this.reason = this.f.Reason.value;
    // this.repor = rep;
    this.description = this.f.Description.value; 
    // console.log("this is reason"+this.repor);
    // console.log("this is reason"+fd);
    this.postReson();
  }


  open(content) {
    // this.deleteId=id;
    // this.deleteThought(id);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
   
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  verify(contentt,id) {
    // this.deleteThought(id);

    this.modalService.open(contentt, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonal(reason)}`;
    });
   
  }
  private getDismissReasonal(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
