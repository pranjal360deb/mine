import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageDeleteComponent } from './manage-delete.component';

describe('ManageDeleteComponent', () => {
  let component: ManageDeleteComponent;
  let fixture: ComponentFixture<ManageDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
