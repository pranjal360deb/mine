import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import { BlankComponent } from './views/blank/blank.component';
import { LoginComponent } from './pages/login/login.component';
import { ProfileComponent } from './views/profile/profile.component';
import { RegisterComponent } from './pages/register/register.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { NonAuthGuard } from './utils/guards/non-auth.guard';
import { VideoComponent } from './views/video/video.component';
import { PhotoComponent } from './views/photo/photo.component';
import { MusicComponent } from './views/music/music.component';
import { PodcastComponent } from './views/podcast/podcast.component';
import { AskComponent } from './views/ask/ask.component';
import { NoteComponent } from './views/note/note.component';
import { StoryComponent } from './views/story/story.component';
import { ArticleComponent } from './views/article/article.component';
import { ThoughtComponent } from './views/thought/thought.component';
import { LoginMainComponent } from './pages/login/login-main/login-main.component';
import { FeedbackComponent } from './views/feedback/feedback.component';
import { IdeaComponent } from './views/idea/idea.component';
import { BugComponent } from './views/bug/bug.component';
import { ManageDeleteComponent } from './views/manage-delete/manage-delete.component';
import { CustomAlert } from './views/custom-alert-table/model/custom-alert';
import { CustomAlertComponent } from './views/custom-alert/custom-alert.component';
import { CreateUsersComponent } from './views/create-users/create-users.component';
import { PeoplesComponent } from './views/peoples/peoples.component';
import { CustomAdsComponent } from './views/custom-ads/custom-ads.component';
import { TagsComponent } from './views/tags/tags.component';
import { AuthGuard} from './pages/guard/auth.guard'

const routes: Routes = [
  {
    path: 'home',
    component: MainComponent,

    children: [
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'blank',
        component: BlankComponent,
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'video',
        component: VideoComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'photo',
        component: PhotoComponent,
        canActivate: [AuthGuard]

      },

      {
        path: 'Music',
        component: MusicComponent,
        canActivate: [AuthGuard]

      },

      {
        path: 'Podcast',
        component: PodcastComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'ask',
        component: AskComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'note',
        component: NoteComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'story',
        component: StoryComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'article',
        component: ArticleComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'thought',
        component: ThoughtComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'feedback',
        component: FeedbackComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'idea',
        component: IdeaComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'bug',
        component: BugComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'delete',
        component: ManageDeleteComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'customAlert',
        component: CustomAlertComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'createUser',
        component: CreateUsersComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'peoples',
        component: PeoplesComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'ads',
        component: CustomAdsComponent,
        canActivate: [AuthGuard]

      },
      {
        path: 'tags',
        component: TagsComponent,
        canActivate: [AuthGuard]

      }


    ],
  },
  {
    path: '',
    component: LoginMainComponent,
    children:[
      {path: '',
      component: LoginComponent,}
    ]
  },
  // {
  //   path: 'register',
  //   component: RegisterComponent,
  // },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
