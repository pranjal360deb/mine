import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import {MatAutocompleteModule} from '@angular/material/autocomplete'
import {MatTableModule} from '@angular/material/table';
import { AppComponent } from './app.component';
import { MainComponent } from './pages/main/main.component';
import { LoginComponent } from './pages/login/login.component';
import { HeaderComponent } from './pages/main/header/header.component';
import { FooterComponent } from './pages/main/footer/footer.component';
import { MenuSidebarComponent } from './pages/main/menu-sidebar/menu-sidebar.component';
import { BlankComponent } from './views/blank/blank.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { ProfileComponent } from './views/profile/profile.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './pages/register/register.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { ToastrModule } from 'ngx-toastr';
import { MessagesDropdownMenuComponent } from './pages/main/header/messages-dropdown-menu/messages-dropdown-menu.component';
import { NotificationsDropdownMenuComponent } from './pages/main/header/notifications-dropdown-menu/notifications-dropdown-menu.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppButtonComponent } from './components/app-button/app-button.component';

import { registerLocaleData, DatePipe } from '@angular/common';
import localeEn from '@angular/common/locales/en';
import { UserDropdownMenuComponent } from './pages/main/header/user-dropdown-menu/user-dropdown-menu.component';
import { VideoComponent } from './views/video/video.component';
import { PhotoComponent } from './views/photo/photo.component';
import { MusicComponent } from './views/music/music.component';
import { PodcastComponent } from './views/podcast/podcast.component';
import { AskComponent } from './views/ask/ask.component';
import { NoteComponent } from './views/note/note.component';
import { StoryComponent } from './views/story/story.component';
import { ArticleComponent } from './views/article/article.component';
import { UserDetailsComponent } from './views/user-details/user-details.component';
import{HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { ThoughtComponent } from './views/thought/thought.component';
import {JwtModule} from '@auth0/angular-jwt';
import { LoginMainComponent } from './pages/login/login-main/login-main.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import {JwtInterceptor} from '../app/pages/Interceptor/jwt-interceptor';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { PhotoCardComponent } from './views/photo-card/photo-card.component';
import { VideoCardComponent } from './views/video-card/video-card.component';
import { MusicCardComponent } from './views/music-card/music-card.component';
import { PodcastCardComponent } from './views/podcast-card/podcast-card.component';
import { StoryCardComponent } from './views/story-card/story-card.component';
import { ArticleCardComponent } from './views/article-card/article-card.component';
import { NoteCardComponent } from './views/note-card/note-card.component';
import { FeedbackComponent } from './views/feedback/feedback.component';
import { FeedbackCardComponent } from './views/feedback-card/feedback-card.component';
import { IdeaCardComponent } from './views/idea-card/idea-card.component';
import { IdeaComponent } from './views/idea/idea.component';
import { BugComponent } from './views/bug/bug.component';
import { BugCardComponent } from './views/bug-card/bug-card.component';
import { ManageDeleteComponent } from './views/manage-delete/manage-delete.component';
import { ManageDeleteTableComponent } from './views/manage-delete-table/manage-delete-table.component';
import { CustomAlertComponent } from './views/custom-alert/custom-alert.component';
import {CustomAlertTableComponent} from './views/custom-alert-table/custom-alert-table.component'
import {NgxUsefulSwiperModule} from 'ngx-useful-swiper';
import { ReportedListComponent } from './views/reported-list/reported-list.component';
import { AskCardComponent } from './views/ask-card/ask-card.component';
import { ThoughtCardComponent } from './views/thought-card/thought-card.component';
import { CreateUsersComponent } from './views/create-users/create-users.component';
import { PeoplesComponent } from './views/peoples/peoples.component';
import { UsersListComponent } from './views/users-list/users-list.component';
import { PeopleListComponent } from './views/people-list/people-list.component';
import { CustomAdsComponent } from './views/custom-ads/custom-ads.component';
import { TagsComponent } from './views/tags/tags.component';
import { TagsListComponent } from './views/tags-list/tags-list.component'
registerLocaleData(localeEn, 'en-EN');

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    MenuSidebarComponent,
    BlankComponent,
    ProfileComponent,
    RegisterComponent,
    DashboardComponent,
    MessagesDropdownMenuComponent,
    NotificationsDropdownMenuComponent,
    AppButtonComponent,
    UserDropdownMenuComponent,
    VideoComponent,
    PhotoComponent,
    MusicComponent,
    PodcastComponent,
    AskComponent,
    NoteComponent,
    StoryComponent,
    ArticleComponent,
    UserDetailsComponent,
    ThoughtComponent,
    LoginMainComponent,
    PhotoCardComponent,
    VideoCardComponent,
    MusicCardComponent,
    PodcastCardComponent,
    StoryCardComponent,
    ArticleCardComponent,
    NoteCardComponent,
    FeedbackComponent,
    FeedbackCardComponent,
    IdeaCardComponent,
    IdeaComponent,
    BugComponent,
    BugCardComponent,
    ManageDeleteComponent,
    ManageDeleteTableComponent,
    CustomAlertComponent,
    CustomAlertTableComponent,
    ReportedListComponent,
    AskCardComponent,
    ThoughtCardComponent,
    CreateUsersComponent,
    PeoplesComponent,
    UsersListComponent,
    PeopleListComponent,
    CustomAdsComponent,
    TagsComponent,
    TagsListComponent
  ],
  imports: [
    NgxUsefulSwiperModule,
    MatTableModule,
    MatAutocompleteModule,
    InfiniteScrollModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AutocompleteLibModule,
    JwtModule,
    NgbModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    NgbModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    DatePipe,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
