import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { AppService } from '../../utils/services/app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../login/auth.service';
// import { AppsiteService } from '../login/appsite.service';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent  {

loginForm: FormGroup;

constructor(private authService: AuthService, private toastr: ToastrService, private formBuilder: FormBuilder, private router: Router) { }

ngOnInit() {
  this.loginForm = this.formBuilder.group({
    UserName: [''],
    Password: ['']
  });
}

get f() { return this.loginForm.controls; }

login() {
  this.authService.login(
    
      this.f.UserName.value,
       this.f.Password.value
    
  )
  .subscribe(success => {
    // success.forEach(element => {
    //   console.log(element);
    // });
    if (success) {
      this.router.navigate(['/home/dashboard']);
      // console.log('success');
    }
  });
  // this.router.navigate(['/dashboard']);
}
}
