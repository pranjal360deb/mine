import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpRequest, HttpHandler } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { map, catchError, finalize, filter, take, switchMap,mapTo, tap } from "rxjs/operators";
import { Observable, of, BehaviorSubject, throwError } from "rxjs";
import { ToastrService } from 'ngx-toastr';
// import { AppsiteService, AppsiteRegistrationModel } from '../login/appsite.service';
import{Tokens} from '../login/Token';

// import { RequestHeader } from "../model/request-header.model";
@Injectable({
  providedIn: 'root'
})
export class AuthService {

    private readonly JWT_TOKEN = 'JWT_TOKEN';
  private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';
  private loggedUser: string;

  constructor(private http: HttpClient,private toastr: ToastrService,) {}
  login( UserName: string, Password: string ): Observable<any> {
    let formData = new FormData
    formData.append("UserName",UserName)
    formData.append("Password",Password)

   let Url= environment.baseURL+"/Auth"
    return this.http.post<any>(Url,formData)
      .pipe(
        tap(tokens =>{
          this.doLoginUser(UserName,tokens);
          // console.log(tokens);
        }),
        // tap(
        //   tokens => this.doLoginUser(UserName, tokens)),
        mapTo(true),
        catchError(error => {
          // alert(error.error);
          this.toastr.error('Please Try Again', 'Something Went Wrong',{
            timeOut: 3000
        } );
          return of(false);
        })
       );
  }

  // logout() {
  //   return this.http.post<any>(this.Url+'/logout', {
  //     'refreshToken': this.getRefreshToken()
  //   }).pipe(
  //     tap(() => this.doLogoutUser()),
  //     mapTo(true),
  //     catchError(error => {
  //       alert(error.error);
  //       return of(false);
  //     }));
  // }

  isLoggedIn() {
    return !!this.getJwtToken();
  }

  // refreshToken() {
  //   return this.http.post<any>(this.Url+'/logout', {
  //     'refreshToken': this.getRefreshToken()
  //   }).pipe(tap((tokens: Tokens) => {
  //     this.storeJwtToken(tokens.jwt);
  //   }));
  // }
  getToken() {
    return localStorage.getItem("LoggedInUser")
  }
  loggedIn() {
    return this.getJwtToken() !== null;
  }

  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  private doLoginUser(UserName: string, tokens) {
    this.loggedUser = UserName;
    this.storeTokens(tokens,this.loggedUser);
  }

  private doLogoutUser() {
    this.loggedUser = null;
    this.removeTokens();
  }

  private getRefreshToken() {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  private storeJwtToken(jwt: string) {
    localStorage.setItem(this.JWT_TOKEN, jwt);
  }

  private storeTokens(tokens,UserName) {
    localStorage.setItem(this.JWT_TOKEN, tokens.token);
    localStorage.setItem("userName", UserName);

  }

  private removeTokens() {
    localStorage.removeItem(this.JWT_TOKEN);
    localStorage.removeItem(this.REFRESH_TOKEN);
  }

}
