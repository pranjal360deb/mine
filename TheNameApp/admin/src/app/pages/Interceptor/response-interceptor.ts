// // import { Injectable, ErrorHandler } from "@angular/core";
// // import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from "@angular/common/http";
// // import { AuthService } from "../login/auth.service";
// // import { Observable, throwError } from "rxjs";
// // import { catchError } from "rxjs/operators";

// // @Injectable()
// // export class ResponseInterceptor implements HttpInterceptor {
// //     constructor(private authService: AuthService) { }
// //     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
// //         // return next.handle(request).pipe(catchError(error => {
// //         //     if (error instanceof HttpErrorResponse) {
// //         //         switch (error.status) {
// //         //             case 401: return this.authService.handle401Error(request, next, error);
// //         //             case 400: return this.authService.handle400Error(error);
// //         //             default:
// //         //                 break;

// //         //         }
// //         //     } else {
// //         //         return throwError(error);
// //         //     }
// //         // }))

// //     }
// // }


// import { Injectable, ErrorHandler } from "@angular/core";
// import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "../login/auth.service";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(error => {
            if (error instanceof HttpErrorResponse) {
                switch (error.status) {
                    // case 401: return this.authService.handle401Error(request, next, error);
                    // case 400: return this.authService.handle400Error(error);
                    default:
                        break;

                }
            } else {
                return throwError(error);
            }
        }))

    }
}