import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  Output,
  EventEmitter,
} from '@angular/core';
import { AppService } from 'src/app/utils/services/app.service';
import * as $ from 'jquery';
import * as AdminLte from 'admin-lte';
import {UsersCreateService} from '../../../views/create-users/services/users-create.service'
@Component({
  selector: 'app-menu-sidebar',
  templateUrl: './menu-sidebar.component.html',
  styleUrls: ['./menu-sidebar.component.scss'],
})
export class MenuSidebarComponent implements OnInit, AfterViewInit {
  @ViewChild('mainSidebar', { static: false }) mainSidebar;
  @Output() mainSidebarHeight: EventEmitter<any> = new EventEmitter<any>();
  userName:string;
  public adminDetails = [];
  openCreate: boolean =false;
  role;
  constructor(public appService: AppService,private UsersCreateService: UsersCreateService) {}
open:boolean = false;
  ngOnInit() {
    // $(document).ready(() => {
    //   const trees: any = $('[data-widget="tree"]');
    //   trees.tree();
    // });
    // $('ul').Treeview(options)
    // console.log(localStorage.getItem("userName"));
    this.userName=localStorage.getItem("userName");
    this.getDetails()
  }
getDetails(){
  this.UsersCreateService.getAdminDetail().subscribe(data=>{
      if(data.roleId!=1){
              this.openCreate=true;
      }
  })
}
  ngAfterViewInit() {
    this.mainSidebarHeight.emit(this.mainSidebar.nativeElement.offsetHeight);
    $('[data-widget="treeview"]').each(function() {
      AdminLte.Treeview._jQueryInterface.call($(this), 'init');
  });
  }

  hide(){
    this.open = false;
  }
}
