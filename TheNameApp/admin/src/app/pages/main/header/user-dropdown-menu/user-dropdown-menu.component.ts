import {
  Component,
  OnInit,
  ViewChild,
  HostListener,
  ElementRef,
  Renderer2,
} from '@angular/core';
import { AppService } from 'src/app/utils/services/app.service';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { MustMatch } from './must-match.validator';
import {UsersCreateService}  from '../../../../views/create-users/services/users-create.service'
@Component({
  selector: 'app-user-dropdown-menu',
  templateUrl: './user-dropdown-menu.component.html',
  styleUrls: ['./user-dropdown-menu.component.scss'],
})
export class UserDropdownMenuComponent implements OnInit {
  changePass: FormGroup;
  registerForm: FormGroup;
  submitted = false;
  public user;
  userName:string;
  closeResult=''
  passwd:any;
  conpasswd:any;
  ExistingPasswd;
  @ViewChild('dropdownMenu', { static: false }) dropdownMenu;
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.hideDropdownMenu();
    }
  }

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private appService: AppService,
    private router: Router,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private UsersCreateService: UsersCreateService
  ) {}

  ngOnInit(): void {
    this.user = this.appService.user;
    this.userName=localStorage.getItem("userName");
    this.changePass = this.formBuilder.group({
      // FromDate: '',
      // ToDate: '',
      // reported: [],
      // name:'',
      // corelationId:'',
      password:['',Validators.required],
      confirmPassword:['',Validators.required],
      // delete:false
    });

    this.registerForm = this.formBuilder.group({
      // title: ['', Validators.required],
      // firstName: ['', Validators.required],
      // lastName: ['', Validators.required],
      ExistingPassword: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
  }, {
      validator: MustMatch('password', 'confirmPassword')
  });

  }
  // get confirmation() { return this.changePass.controls; }
  password(){
    this.UsersCreateService.changePassword(this.ExistingPasswd,this.passwd).subscribe(data=>{
      // console.log(data)
      this.modalService.dismissAll()
      this.toastr.success('Update Successfully', 'Your Password', {
        timeOut: 3000
    });
    })
    // console.log("password confirmation successfull");
  }

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control); }
      else { control.markAsTouched(); };
    });
  }
  // passwordConfirm(){

  //   this.passwd=this.confirmation.password.value;
  //   this.conpasswd=this.confirmation.confirmPassword.value;
  //   if(this.passwd===this.conpasswd){
  //     this.password();
  //   }
  //   else{
  //     console.log("password does not match");
  //     this.toastr.error("Doesn't match", 'Your Password', {
  //       timeOut: 3000
  //   });
  //   }

  // }
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }
    else{
      this.ExistingPasswd=this.f.ExistingPassword.value
      this.passwd=this.f.password.value;
      this.conpasswd=this.f.confirmPassword.value;
      // console.log(this.passwd,this.conpasswd)
      this.password();
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
    }

    // display form values on success
}
  reportUser(reportUserList) {
    // this.deleteId=id;
    // this.reason();
    // this.deleting = boolValue;
    // this.nameValue=name
    // console.log(boolValue);
    // this.deleteThought(id);
    // this.filterForm.get('delete').setValue(del);

    this.modalService.open(reportUserList, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReasonReported(reason)}`;
    });
   
  }
  private getDismissReasonReported(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  toggleDropdownMenu() {
    if (this.dropdownMenu.nativeElement.classList.contains('show')) {
      this.hideDropdownMenu();
    } else {
      this.showDropdownMenu();
    }
  }

  showDropdownMenu() {
    this.renderer.addClass(this.dropdownMenu.nativeElement, 'show');
  }

  hideDropdownMenu() {
    this.renderer.removeClass(this.dropdownMenu.nativeElement, 'show');
  }

  logout() {
    localStorage.clear();
    this.router.navigateByUrl('');
  }
}
